-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 25, 2019 at 09:53 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_organ_donation`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events`
--

CREATE TABLE `tbl_events` (
  `events_id` tinyint(3) UNSIGNED NOT NULL,
  `events_title` varchar(150) NOT NULL,
  `events_description` text NOT NULL,
  `events_location` varchar(120) NOT NULL,
  `events_date` date NOT NULL,
  `events_img` varchar(255) NOT NULL,
  `events_resized_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_events`
--

INSERT INTO `tbl_events` (`events_id`, `events_title`, `events_description`, `events_location`, `events_date`, `events_img`, `events_resized_img`) VALUES
(4, 'PEDESTRIAN SUNDAY', 'Community Street Festival: Human beings feel at their best in vibrant, human scale, friendly environments. Pedestrian Sundays allows us all to flourish in just such a space.<br />\r\n<br />\r\nWe will be visiting with our interactive glass display for residents to leave their mark! Come visit us to learn more about organ donation myths and misconceptions, and register on the spot.', 'Kensington Market, Toronto, Ontario.', '2019-06-24', '1553531243_2417.png', 'th_1553531243_2417.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_myths`
--

CREATE TABLE `tbl_myths` (
  `myths_id` tinyint(3) UNSIGNED NOT NULL,
  `myths_title` varchar(120) NOT NULL,
  `myths_desc` text NOT NULL,
  `myths_img` varchar(255) NOT NULL,
  `myths_resized_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_myths`
--

INSERT INTO `tbl_myths` (`myths_id`, `myths_title`, `myths_desc`, `myths_img`, `myths_resized_img`) VALUES
(1, 'Myth One ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum ipsum ac sem condimentum, et viverra est luctus. Phasellus sodales aliquet mattis. Phasellus lorem purus, dapibus sit amet ultricies ut, aliquam a leo. Cras tortor velit, condimentum a ipsum a, varius vulputate lectus. Morbi nisi libero, pulvinar a interdum nec, luctus vitae diam. Fusce ultricies vulputate metus et porta. Ut at pretium tellus, ut tincidunt orci. Duis ut hendrerit nibh.<br /><br />Sed nibh leo, pharetra quis nisl vel, vehicula posuere tellus. Aenean non risus nunc. Nunc lobortis efficitur vehicula. Duis eu pellentesque ipsum, id blandit enim. Cras quis est ac purus ultrices condimentum ut in arcu. Pellentesque ullamcorper lectus ut velit luctus suscipit. Sed commodo ligula et nunc congue, et aliquam dui varius. Sed in orci vulputate, convallis urna a, rhoncus odio. ', 'test_1.jpg', NULL),
(2, 'Myth two', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum ipsum ac sem condimentum, et viverra est luctus. Phasellus sodales aliquet mattis. Phasellus lorem purus, dapibus sit amet ultricies ut, aliquam a leo. Cras tortor velit, condimentum a ipsum a, varius vulputate lectus. Morbi nisi libero, pulvinar a interdum nec, luctus vitae diam. Fusce ultricies vulputate metus et porta. Ut at pretium tellus, ut tincidunt orci. Duis ut hendrerit nibh.<br /><br />Sed nibh leo, pharetra quis nisl vel, vehicula posuere tellus. Aenean non risus nunc. Nunc lobortis efficitur vehicula. Duis eu pellentesque ipsum, id blandit enim. Cras quis est ac purus ultrices condimentum ut in arcu. Pellentesque ullamcorper lectus ut velit luctus suscipit. Sed commodo ligula et nunc congue, et aliquam dui varius. Sed in orci vulputate, convallis urna a, rhoncus odio. ', 'img_60.png', NULL),
(4, 'Test', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam mollis ultricies mi, a rhoncus urna condimentum a. Sed molestie, libero at facilisis condimentum, nisl lacus ultrices turpis, nec sagittis odio tortor pharetra turpis. Duis lacinia nisi metus, non fringilla ligula commodo non. Vivamus condimentum vel dui et maximus. Donec commodo sodales eros, quis efficitur eros porta sed. Cras non varius urna. Sed vel elit commodo, lobortis nisl vel, malesuada lectus. Morbi ac cursus mauris, vitae mollis nibh. Nulla consequat, mi a egestas consectetur, magna justo auctor tellus, id pulvinar nulla dui fringilla lectus. Proin vitae ante vel nisi laoreet dignissim ut quis metus. Praesent ultricies arcu sapien, non pharetra lectus malesuada in. Ut tincidunt feugiat viverra.<br />\r\n<br />\r\nEtiam in massa et libero pharetra euismod. Donec turpis ex, interdum sit amet sapien finibus, aliquet euismod sem. Aenean faucibus ullamcorper justo, eu varius sapien bibendum in. Aliquam erat volutpat. Morbi congue enim ante, nec euismod augue ultricies in. Integer mattis est ut odio dictum eleifend. Nam erat elit, interdum in lectus id, placerat mollis tellus. Nunc elementum odio massa, bibendum convallis ipsum tempus at. Integer vitae leo ac est scelerisque consequat non euismod eros. In hac habitasse platea dictumst. Pellentesque interdum facilisis mauris. Phasellus consectetur, enim sed pulvinar posuere, libero risus maximus ligula, mollis accumsan massa dui ullamcorper elit. Quisque sit amet ex dapibus, congue magna et, euismod leo. Pellentesque ut ligula ornare, laoreet turpis in, hendrerit urna. Quisque eu erat eu sapien tincidunt elementum. Phasellus pretium pharetra justo quis bibendum.', '1553278385_5461.jpeg', 'th_1553278385_5461.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_site_images`
--

CREATE TABLE `tbl_site_images` (
  `images_id` smallint(5) UNSIGNED NOT NULL,
  `images_name` varchar(255) NOT NULL,
  `images_alt_desc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stories`
--

CREATE TABLE `tbl_stories` (
  `stories_id` tinyint(3) UNSIGNED NOT NULL,
  `stories_title` varchar(150) NOT NULL,
  `stories_body` text NOT NULL,
  `stories_img` varchar(255) NOT NULL,
  `stories_resized_img` varchar(255) DEFAULT '',
  `stories_date` date NOT NULL,
  `stories_person_name` varchar(120) NOT NULL,
  `stories_person_age` varchar(50) NOT NULL,
  `stories_category` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_stories`
--

INSERT INTO `tbl_stories` (`stories_id`, `stories_title`, `stories_body`, `stories_img`, `stories_resized_img`, `stories_date`, `stories_person_name`, `stories_person_age`, `stories_category`) VALUES
(8, 'Jordan Gilbert\'s Story', 'We had the privilege of talking to Wesley and Maya Gilbert, the father and sister of Jordan Gilbert, who as a baby was born with biliary atresia, a disease that prevents bile from flowing properly out of the body, and as a result needed to receive an organ donation to live. Jordan received two transplants. Jordan was number one on the wait list in North America for 21 days before receiving his second transplant, and the transplant was unsuccessful in saving his life.<br />\r\n<br />\r\nWesley and Maya reflected on their favourite moments with Jordan. Maya started by saying that “whenever he was sad, he would squeeze my pinkie.” Wesley’s favourite memory is when he took a picture of the biggest smile Jordan ever had in their van, at a time before his first transplant when he was usually in a lot of pain and discomfort and so didn’t smile very often. The Gilberts have the picture hanging up in their living room to this day.<br />\r\n<br />\r\nWesley said the health professionals at Children’s Hospital that were involved in Jordan’s care were family to the Gilberts. They explained the problems that Jordan was experiencing, made them feel like they were a part of the team, and were trustworthy and reliable. <br />\r\n<br />\r\nWhen asked about what he’d say to someone unsure about registering as an organ donor, Wesley shared that if there were more people on the list, Jordan’s complications while waiting may not have been as severe and he might still be here today. He said that it is incredible to receive an organ donation to know someone else gave your family the gift of life, and until you’ve walked the mile in another family’s shoes, it’s so important to offer that chance to them.<br />\r\n<br />\r\n&quot;It’s really endless what you can give and what in life you can give to somebody.” ', '1553487346_3670.png', 'th_1553487346_3670.png', '2019-03-25', 'Jordan Gilbert', '1', 'Recipient'),
(9, 'Owen\'s Story', 'Story content', '1553262863_3301.png', 'th_1553262863_3301.png', '2019-03-12', 'Owen', '28', 'Donor'),
(10, 'Ruth\'s Story', 'Story Content goes here..', '1553262879_4795.png', 'th_1553262879_4795.png', '2019-03-11', 'Ruth', '30', 'Donor'),
(11, 'Jasper\'s Story', 'Story content goes here...', '1553262898_3396.png', 'th_1553262898_3396.png', '2019-03-18', 'Jasper', '40', 'Recipient'),
(12, 'Adelaide\'s Story', 'Story content goes here...', '1553262916_9491.png', 'th_1553262916_9491.png', '2019-03-25', 'Adelaide', '31', 'Donor'),
(13, 'Tina\'s Story', 'Story content goes here...', '1553262934_2216.png', 'th_1553262934_2216.png', '2019-03-04', 'Tina', '24', 'Recipient'),
(14, 'Brooke\'s Story', 'Story content goes here...', '1553262999_5524.png', 'th_1553262999_5524.png', '2019-03-18', 'Brooke', '40', 'Donor'),
(15, 'Tom\'s Story', 'Story content goes here...', '1553263027_3954.png', 'th_1553263027_3954.png', '2019-03-07', 'Tom', '45', 'Recipient');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` tinyint(3) UNSIGNED NOT NULL,
  `user_fname` varchar(180) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_email` varchar(120) NOT NULL,
  `user_img` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `user_active` tinyint(1) NOT NULL DEFAULT '1',
  `user_last_login` datetime DEFAULT NULL,
  `user_failed_login_attempts` tinyint(4) NOT NULL DEFAULT '0',
  `user_admin_role` tinyint(1) NOT NULL DEFAULT '1',
  `user_ip` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_fname`, `user_name`, `user_password`, `user_email`, `user_img`, `user_active`, `user_last_login`, `user_failed_login_attempts`, `user_admin_role`, `user_ip`) VALUES
(1, 'admin', 'admin', '$2y$10$8G7D7bd9U9nDqp/gRAiZqe6KoCz7dEpA0GpT3XtGa5aeqklbrYKXW', 'test@test.ca', 'default.jpg', 1, '2019-03-25 17:52:46', 0, 1, '::1'),
(9, 'test update', 'test update', '$2y$10$BJpQZp1vAXo9TgSEARhap.yJatPcV2rbfJwcau4eo3fRXgTQg4RFS', 'camillo@test.com', 'default.jpg', 1, NULL, 6, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_events`
--
ALTER TABLE `tbl_events`
  ADD PRIMARY KEY (`events_id`);

--
-- Indexes for table `tbl_myths`
--
ALTER TABLE `tbl_myths`
  ADD PRIMARY KEY (`myths_id`);

--
-- Indexes for table `tbl_site_images`
--
ALTER TABLE `tbl_site_images`
  ADD PRIMARY KEY (`images_id`);

--
-- Indexes for table `tbl_stories`
--
ALTER TABLE `tbl_stories`
  ADD PRIMARY KEY (`stories_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_events`
--
ALTER TABLE `tbl_events`
  MODIFY `events_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_myths`
--
ALTER TABLE `tbl_myths`
  MODIFY `myths_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_site_images`
--
ALTER TABLE `tbl_site_images`
  MODIFY `images_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_stories`
--
ALTER TABLE `tbl_stories`
  MODIFY `stories_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

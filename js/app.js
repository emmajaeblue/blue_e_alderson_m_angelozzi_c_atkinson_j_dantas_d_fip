// ----------- LOTTIE bodymovin script ------------- //
let animation = bodymovin.loadAnimation({
  container: document.getElementById("bm"),
  render: "svg",
  loop: false,
  autoplay: true,
  path: "js/data.json"
});
// Avoid Lottie to load in other pages since it is outside of the Vue instance
if (/stories/.test(window.location.href)) {
  document.querySelector('#bm').style.display = 'none';
}
if (/myths/.test(window.location.href)) {
  document.querySelector('#bm').style.display = 'none';
}
if (/events/.test(window.location.href)) {
  document.querySelector('#bm').style.display = 'none';
}
if (/media/.test(window.location.href)) {
  document.querySelector('#bm').style.display = 'none';
}
if (/details/.test(window.location.href)) {
  document.querySelector('#bm').style.display = 'none';
}
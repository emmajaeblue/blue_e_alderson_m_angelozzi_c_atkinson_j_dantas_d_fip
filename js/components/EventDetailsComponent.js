import HeaderComponent from "./layout/HeaderComponent.js";
import FooterComponent from "./layout/FooterComponent.js";

export default {
  template: /*html */`
  <section>
    <HeaderComponent />
    <h2 class="hidden">Organ Donation Events Details Page</h2>
    <section id="events-hero">
      <h2 class="hidden">Organ Donation Events Details Hero Area</h2>
        <div class="details-img">
          <div class="gradient"></div>
          <div class="overlay-txt">
          <i class="fas fa-map-marker-alt fa-3x"></i>
          <p>PEDESTRIAN SUNDAY, Toronto, Ontario</p>
          </div>
        </div>
        <div class="only-dkt hero-info">
        <img src="images/wordmark_logo.png" alt="Wordmark Logo">
   
         <p>We’re building a community of people who want to leave their mark on the world. That means reaching out in real life to discuss the facts on organ donation, and work through misconceptions and misunderstandings together.</p>
        <a class="btn" href="https://www.beadonor.ca/community/toronto">Register</a>
        </div>
        <div class="mobile-only pentagon">
        <img src="images/wordmark_logo_white.png" alt="Wordmark Logo">
        <p>at our events</p>
        </div>
        <div class="dkt-newsletter">
            <form >
              <h4>stay up to date with the latest events</h4>
              <input type="email" name="email" placeholder="Your Email Goes Here">
              <button class="btn">subscribe</button>
            </form>
          </div>
          
    </section>
    <section id="event-details" class="section-container">
      <h2> #leaveyourmark</h2>
      <h3>with us in <span class="green-text">Toronto</span></h3>
      <div class="event-pics">
        <img src="images/eventDetails-image1.png" alt="Toronto Cityscape from the Lake">
        <img src="images/eventDetails-image2.png" alt="Colourful Paint in plastic cups">
        <img src="images/eventDetails-image3@2x.png" alt="Father carrying daughter over his shoulders">
        <img src="images/eventDetails-image4.png" alt="Woman walking her dog">
      </div>
      <div class="info-container">
        <div class="fingerPrints">
          <img src="images/fingerPrints.png" alt="Fingerprints">
        </div>
        <div class="event-info">
          <h4> Pedestrian Sunday -  Kensington Market</h4>
          <p>Community Street Festival: Human beings feel at their best in vibrant, human scale, friendly environments. Pedestrian Sundays allows us all to flourish in just such a space.
           </p>
           <p>We will be visiting with our interactive glass display for residents to leave their mark! Come visit us to learn more about organ donation myths and misconceptions, and register on the spot. </p>
          <!-- <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloremque ipsam maxime exercitationem tempora!<span class="green-text">  Vel odio vero dolorem odit esse laborum, aliquid optio recusandae molestiae </span>quasi atque possimus officiis facere maxime!</p> -->
        </div>
      </div>
    </section>
   
    <section class="call-action section-container">
      <div class="text">
      <h4> MAKE A DIFFERENCE BY GETTING INVOLVED.</h4>
      
      <p>We’re always looking for people to come out and volunteer with us. <a class="green-text" href="#"> If you’re interested, contact us about volunteering at this event! </a></p>
        <p>If you can’t make an event but still want to contribute, you can still leave your mark. <span class="green-text"> leave your mark </span></p>
        <a class="btn" href="https://www.beadonor.ca/community/toronto">Register</a>
        </div>
        <div class="picture">
          <img src="images/polaroids@2x.png" alt="Polaroid Picture">
        </div>
    </section>
    <div class="section-container">
      <h4 class="social-title">share this event</h4>
      <div class="social-media">
      <i class="fab fa-twitter-square fa-3x"></i>
      <i class="fab fa-facebook fa-3x"></i>
      <i class="fas fa-envelope-square fa-3x"></i>
      </div>
    </div> 
    <div class="newsletter">
            <form >
              <h4>stay up to date with the latest events</h4>
              <input type="email" name="email" placeholder="Your Email Goes Here">
              <button class="btn">subscribe</button>
            </form>
          </div>
    <FooterComponent />
  </section>
  `,
  components: {
    HeaderComponent: HeaderComponent,
    FooterComponent: FooterComponent
  }
}
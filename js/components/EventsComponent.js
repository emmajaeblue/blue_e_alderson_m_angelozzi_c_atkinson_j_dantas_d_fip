import HeaderComponent from "./layout/HeaderComponent.js";
import FooterComponent from "./layout/FooterComponent.js";

export default {
  template: /*html */`
  <section>
    <HeaderComponent />
    <h2 class="hidden">Organ Donation Events Page</h2>
    <section id="events-hero">
      <h2 class="hidden">Organ Donation Events Hero Area</h2>
        <div class="hero-img">
          <div class="gradient"></div>
          <div class="overlay-txt">
          <i class="fas fa-map-marker-alt fa-3x"></i>
          </div>
        </div>
        <div class="only-dkt hero-info">
        <img src="images/wordmark_logo.png" alt="Wordmark Logo">
        <p>Want to learn about organ donation, involve your friends and family in exploring more, and meet other people who want to make their mark? We're here to help! Come to one of our events in your city to chat and discover more.</p>
        <a class="btn" href="https://www.beadonor.ca/community/toronto">Register</a>
        </div>
        <div class="mobile-only pentagon">
        <img src="images/wordmark_logo_white.png" alt="Wordmark Logo">
        <p>At one of our events</p>
        </div>
        <div class="dkt-newsletter">
            <form >
              <h4>stay up to date with the latest events</h4>
              <input type="email" name="email" placeholder="Your Email Goes Here">
              <button class="btn">subscribe</button>
            </form>
          </div>
          <div class="dsk-blob">

            <h4>Leaving <span class="green-text">your mark</span> means <span class="green-text">connecting</span> and <span class="green-text"> sharing</span> together.</h4>
            <img src="images/img_2.png" alt="red fingerprint">
            <p>To save lives, we need to debunk the myths. <span class="green-text">With play, with story, and with conversation </span>to unpack organ donation and why it matters.<span class="green-text"> Join us in person </span> to start the journey together.</p>
          </div>
    </section>
    <section id="upcoming-events" class="section-container">
      <h2> Upcoming Events</h2>
      <div class="events-container">
        <!-- Implement v-for with dynamic data -->
      <router-link to="/details" > 
        <div class="event-tile thumb1">
        <img v-if="events.length" :src="'images/'+ events[0].events_img" alt="event1">
          <div class="evt-info">
            <p class="date">24<span>jun</span></p>
            <p class="location">toronto<span>KENSINGTON MARKET</span></p>
          </div>
        </div>
        </router-link>
        <div class="event-tile thumb2">
        <img src="images/event2-thumbnail.png" alt="event2">
          <div class="evt-info">
            <p class="date">18<span>aug</span></p>
            <p class="location">ottawa<span>street festival</span></p>
          </div>
        </div>
        <div class="event-tile thumb3">
        <img src="images/event3-thumbnail.png" alt="event3">
          <div class="evt-info">
            <p class="date">04<span>sep</span></p>
            <p class="location">london<span>western frosh</span></p>
          </div>
        </div>
        <div class="event-tile thumb4">
        <img src="images/event4-thumbnail.png" alt="event4">
          <div class="evt-info">
            <p class="date">28<span>sep</span></p>
            <p class="location">milton<span>arts market</span></p>
          </div>
        </div>
        <a href="#">see all events +</a>
      </div>
    </section>
    <hr class="line">
    <section id="event-invite" class="section-container">
      <h2> Want to include us somewhere?</h2>
      <div class="thumb-wrapper">
        <div class="thumbnail-tile">
            <img class="thumb" src="images/college-thumbnail.png" alt="College Thumbnail">
            <h3>post-secondary</h3>
            <p>Want to start an organ donor registration campaign at your college or university? We're here to help! We're happy to co-run an event with your soph team or student club, or bring a speaker to a pre-existing event.</p>
            <a class="btn" href="#">Learn More</a>
          </div>
          <div class="thumbnail-tile">
            <img class="thumb" src="images/family-thumbnail.png" alt="Family Thumbnail">
            <h3>family</h3>
            <p>We run playful, age-appropriate events to involve the whole family in conversations about organ donation. Our interactive displays include fingerpainting and debunking myths that all ages can have fun with.</p>
            <a class="btn" href="#">make memories</a>
          </div>
          <div class="thumbnail-tile">
            <img class="thumb" src="images/community-thumbnail.png" alt="Family Thumbnail">
            <h3>community</h3>
            <p>Running a local festival, market, or other community event where you want to give people the chance to leave their mark? We'd love to be a part of the adventure. Contact us so we can partner together. </p>
            <a class="btn" href="#">get involved</a>
          </div>
      </div>
    </section>
    <div class="newsletter">
            <form >
              <h4>stay up to date with the latest events</h4>
              <input type="email" name="email" placeholder="Your Email Goes Here">
              <button class="btn">subscribe</button>
            </form>
          </div>
    <FooterComponent />
  </section>
  `,
  data() {
    return {
      details: '',
      events: []
    }
  },
  mounted: function () {
    this.getEvents();
  },
  methods: {
    getEvents() {
      let url = "./admin/getevents.php";
      console.log(url);

      fetch(url) // pass in the one or many query
        .then(res => res.json())
        .then(data => {
          console.log(data);
          this.events = data;
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  },
  components: {
    HeaderComponent: HeaderComponent,
    FooterComponent: FooterComponent
  }
}
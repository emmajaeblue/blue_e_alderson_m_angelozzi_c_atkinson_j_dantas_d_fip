import HeaderComponent from "./layout/HeaderComponent.js";
import FooterComponent from "./layout/FooterComponent.js";

export default {
  template: /*html */`
  <section>
    <HeaderComponent />
    <h2 class="hidden">Organ Donation Story Page</h2>
      <section id="media-hero" class="hero-section">
      <h2 class="hidden">Organ Donation Stories Hero Area</h2>
      <div class="gradient"></div>
        <div class="img">
         <img  class="mobile-only" src="images/media.png" alt="media Image">
         <img  class="mobile-hide" src="images/media@2x.png" alt="media Image">
        </div>
       <div class="teaser">
       <img id="wrd-logo" src="images/wordmark_logo.png" alt="Wordmark Logo">
         <h2>JOIN THE MOVEMENT</h2>
         <h3>AND BRING AWARENESS AND SAVE LIVES OR SOMETHING</h3>
         <ol>
           <li>TAKE A PICTURE</li>
           <li>USE OUR HASHTAG #LEAVEYOURMARK</li>
           <li>POST AND SHARE</li>
         </ol>
       </div>
    </section>
    <section class="section-container" id="media-thumbs">
     <h2>JOIN THE <span class="green-text">178,450</span> PEOPLE WHO ARE PART OF <span class="bold">#LEAVEYOURMARK</span> </h2>
     <div class="thumbs">

      <div class="pic" v-for="image in images">
        <img :src="'images/' + image" alt="image">
      </div>

    </div>


    </section>
    <section class="section-container">
      <h2>Are you representing us?</h2>
      <div class="files">
        <div class="download">
        <img src="images/heart_logo.png" alt="Organ Donation Logo">
          <a href="images/heart_logo.png" download>
            <i class="fas fa-download fa-3x"></i>
          </a>
        </div>
        <div class="download">
        <img id="wrd-logo" src="images/wordmark_logo.png" alt="Wordmark Logo">
        <a href="images/wordmark_logo.png" download>
            <i class="fas fa-download fa-3x"></i>
          </a>
        </div>
        <div class="download">
        <img id="wrd-logo" src="images/idea_logo.png" alt="Logo">
        <a href="images/idea_logo.png" download>
            <i class="fas fa-download fa-3x"></i>
          </a>
        </div>
      </div>

    </section>
    <div class="dkt-newsletter">
            <form >
              <h4>stay up to date with the latest events</h4>
              <input type="email" name="email" placeholder="Your Email Goes Here">
              <button class="btn">subscribe</button>
            </form>
          </div>
   <FooterComponent />
  </section>

  `,
  data() {
    return {
      images: ['media1.png', 'media2.png', 'media3.png', 'media4.png', 'media5.png', 'media6.png', 'media7.png', 'media8.png']
    }
  },
  mounted: function () {


  },
  methods: {

  },
  components: {
    HeaderComponent: HeaderComponent,
    FooterComponent: FooterComponent
  }
}

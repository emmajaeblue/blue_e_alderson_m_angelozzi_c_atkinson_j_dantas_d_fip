import HeaderComponent from "./layout/HeaderComponent.js";
import FooterComponent from "./layout/FooterComponent.js";

export default {
  template: /*html */`
  <section>
    <HeaderComponent />
    <h2 class="hidden">Organ Donation Stories Page</h2>
    <section id="stories-hero">
      <h2 class="hidden">Organ Donation Stories Hero Area</h2>
      <!-- <div class="gradient" @mouseover="showControls" @mouseout="hideControls"></div> -->
        <div class="video">
        <div class="fullscreen-vid-wrap">
            <div class="vid-controls">
              <div class="ctr-wraper">
                <div class="icon" @click="volOff()">
                <i class="fas fa-volume-off fa-3x"></i>
                </div>
                <div class="icon" @click="volOn()">
                <i class="fas fa-volume-up fa-3x"></i>
                </div>
                <div class="icon" @click="restart()">
                <i class="fas fa-undo-alt fa-3x"></i>
                </div>
                <div class="icon" v-show="playing" @click="pause()">
                <i class="fas fa-pause fa-3x" ></i>
                </div>
                <div class="icon"v-show="!playing" @click="play">
                <i class="fas fa-play fa-3x"></i>
                </div>

                </div>
            </div>
            <video id="vid" autoplay muted loop>
            </video>
          </div>
         <!-- <video src=""></video> -->
         <!-- <img class="mobile-hide" src="images/main-stories@2x.png" alt="">
         <img class="mobile-only" src="images/main-stories-mobile.png" alt=""> -->
        </div>
        <div class="small-only subtitle">
          <h3>1 donor can save up to 8 lives.</h3>
        </div>
        <div class="mobile-only action">
          <a class="btn" href="https://www.beadonor.ca/community/toronto">Register</a>
          <p>browse all stories</p>
          <img id="arrow-down" src="images/arrow_down.svg" alt="Arrow Down">
        </div>
        <div class="only-dkt hero-info">
        <img id="wrd-logo" src="images/wordmark_logo.png" alt="Wordmark Logo">
        <p>So many people's lives are impacted by organ donation: from organ recipients and their families, to the families of organ donors, to those who never receive a donation. Take the time to listen to their stories and what they have to say.</p>
        <a class="btn" href="https://www.beadonor.ca/community/toronto">Register</a>
        </div>
         
    </section>
    <div class="dkt-newsletter">
            <form >
              <h4>stay up to date with the latest stories</h4>
              <input type="email" name="email" placeholder="Your Email Goes Here">
              <button class="btn">subscribe</button>
            </form>
          </div>
    <section id="stories-thumbs" class="section-container">
      <h2 class="hidden"> Stories Thumbnail Section</h2>
     <!-- Do a v-for for the info that comes from the db???? -->
     
      <div v-if="stories.length" v-for="story in stories" class="story-thumb thumbnail-tile">
      <router-link :to="'/stories/'+story.stories_id" > 
        <div class="pic">
          <img :src="'images/thumbs/' + story.stories_resized_img" alt="story.stories_title">
        </div>
      </router-link>
        <small>{{story.stories_category}}</small>
        <h3>{{story.stories_title}}</h3>
      </div>
     
    </section>
   
    <FooterComponent />
  </section>
  `,
  data() {
    return {
      stories: [],
      vid: null,
      playing: true,
    }
  },
  mounted: function () {
    this.getStories();
    this.vid = document.querySelector('#vid');
    // console.log(this.vid);
    this.showVideo();
  },
  methods: {
    getStories() {
      let url = "./admin/getstories.php";
      console.log(url);

      fetch(url) // pass in the one or many query
        .then(res => res.json())
        .then(data => {
          console.log(data);
          this.stories = data;
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    showVideo() {
      var vid = document.querySelector('#vid');

      // create source video element according to screen size
      if (window.matchMedia("(max-width: 700px)").matches) {
        this.videoSource(vid, "video/gilbert_mobile.mp4", "video/mp4");

      } else if (window.matchMedia("(max-width: 999px)").matches) {
        this.videoSource(vid, "video/gilbert_desktop.mp4", "video/mp4");

      } else if (window.matchMedia("(min-width: 1000px)").matches) {
        this.videoSource(vid, "video/gilbert_desktop.mp4", "video/mp4");

      }
    },

    videoSource(element, src, type) {
      var source = document.createElement("source");

      source.src = src;
      source.type = type;

      element.appendChild(source);
    },

    volOn() {
      // console.log('volOn');
      this.vid.muted = false;
    },

    volOff() {
      // console.log('volOff');
      this.vid.muted = true;
    },
    play() {
      this.vid.play();
      this.playing = true;
      // console.log('play clicked');
    },
    pause() {
      this.vid.pause();
      this.playing = false;
      // console.log('pause clicked');
    },
    restart() {
      this.vid.pause();
      this.vid.currentTime = 0;
      this.vid.play();
    }

  },
  components: {
    HeaderComponent: HeaderComponent,
    FooterComponent: FooterComponent
  }
}
import HeaderComponent from "./layout/HeaderComponent.js";
import FooterComponent from "./layout/FooterComponent.js";

export default {
  template: /*html */`
  <section>
    <HeaderComponent />
    <h2 class="hidden">Organ Donation Story Page</h2>
    <section id="story-hero">
      <h2 class="hidden">Organ Donation Stories Hero Area</h2>
      <div class="gradient"></div>
        <div class="img">
        <img :src="'images/'+ story.stories_img" alt="story.stories_title">
              </div>
        </div>
       <div class="teaser">
         <h2 >{{story.stories_title}}</h2>
         <p>Every day, people of all ages and backgrounds become recipients—and donors—of organs, eyes, and tissues. Lives are transformed. Donor families turn loss into hope. Advocates spread the message. These inspiring stories are presented here so you can see what donation makes possible. </p>
       </div>
       <hr class="mobile-only line">
     
    </section>
    <section class="story-container">
    <article id="story" class="section-container">
      <h2 class="hidden"> Full Story Section</h2>
      <p v-html="body"></p>
        <!-- <div>
        <img class="mobile-only" src="images/polaroid_2.png" alt="polaroid">
        </div>
      
        <div>
        <img class="mobile-only" src="images/polaroid_2.png" alt="polaroid">
        <img class="mobile-only" src="images/polaroid_2.png" alt="polaroid">
        </div>
        <div class="only-dkt imgs">
        <img src="images/polaroid_2.png" alt="polaroid">
        <img src="images/polaroid_2.png" alt="polaroid">
        <img src="images/polaroid_2.png" alt="polaroid">
        </div> -->
     
      </article>
     
      <div class="social-wrapper">
      <h4 class="social-title">share this story</h4>
      <div class="social-media">
      <i class="fab fa-twitter-square fa-3x"></i>
      <i class="fab fa-facebook fa-3x"></i>
      <i class="fas fa-envelope-square fa-3x"></i>
      </div>
      </div>
      <aside>
        <h3 class="aside-title">related stories</h3>
        <div class="thumbs">
        <div v-if="relatedStories.length" v-for="related in relatedStories" class="story-thumb">
              <div class="pic">
                <img :src="'images/thumbs/'+ related.stories_resized_img" alt="related.stories_title">
              </div>
              <small>{{related.stories_category}}</small>
              <h4>{{related.stories_title}}</h4>
            </div>
          
          </div> 
      </aside>
    </section>
    <div class="dkt-newsletter">
            <form >
              <h4>stay up to date with the latest events</h4>
              <input type="email" name="email" placeholder="Your Email Goes Here">
              <button class="btn">subscribe</button>
            </form>
          </div>
    <FooterComponent />
  </section>
  `,
  data() {
    return {
      story: [],
      body: "",
      relatedStories: [],
    }
  },
  mounted: function () {
    this.animOnScroll();
    this.getStory();
  },
  watch: {
    story() {
      // console.log(this.story[0].stories_category);
      let category = this.story.stories_category;
      this.getRelatedStories(category);
    }
  },
  methods: {
    getStory() {

      let params = { id: this.$route.params };

      let query = params.id.id;

      let url = "./admin/getsinglestory.php?id=" + query;
      // console.log(url);

      fetch(url) // pass in the one or many query
        .then(res => res.json())
        .then(data => {
          // console.log(data);
          this.story = data[0];
          this.body = this.story.stories_body;

        })
        .catch(function (error) {
          console.log(error);
        });

    },
    getRelatedStories(category) {

      console.log(category);
      let url = "./admin/getsinglestory.php?category=" + category;
      console.log(url);

      fetch(url) // pass in the one or many query
        .then(res => res.json())
        .then(data => {
          // console.log(data);
          this.relatedStories = data;
        })
        .catch(function (error) {
          console.log(error);
        });

    },
    animOnScroll() {
      // console.log("width: " + window.innerWidth);
      if (window.innerWidth > 1200) {
        window.addEventListener('scroll', function () {
          // console.log(window.scrollY);

          //Get aside
          let aside = document.querySelector('aside');
          // console.log(aside.classList);
          if (window.scrollY > 736) {
            aside.classList.add("fixed");
          }
          if (window.scrollY < 736) {
            aside.classList.remove("fixed");
          }
          if (window.scrollY > 1303) {
            aside.classList.remove("fixed");
          }
        });
      }
    },

  },
  components: {
    HeaderComponent: HeaderComponent,
    FooterComponent: FooterComponent
  }
}
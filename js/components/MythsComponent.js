import HeaderComponent from "./layout/HeaderComponent.js";
import FooterComponent from "./layout/FooterComponent.js";

export default {
  template: /*html */`
  <section>
    <HeaderComponent />
    <h1 class="hidden">Organ Donation Myths Page</h1>
      <section id="myths-hero" class="hero-section">
        <h2 class="hidden">Organ Donation Stories Hero Area</h2>
        <div class="gradient"></div>
          <div class="img">
           <img  class="mobile-only" src="images/mythsHero.png" alt="Myths Image">
           <img  class="mobile-hide" src="images/mythsHero@2x.png" alt="Myths Image">
          </div>
         <div class="teaser hero-info">
         <img id="wrd-logo" class="myth-logo" src="images/wordmark_logo.png" alt="Wordmark Logo">
           <p>There are many misconceptions about organ donation. Leave Your Mark by breaking the stigma and inform youself and others about the facts. Read some of the most common myths around organ donation below.</p>
         </div>
       </section>

       <section id="" class="section-container myths">
        <div class="myths-container">
          <div class="image-container">
            <img src="images/myth1.png" alt="myth image">
          </div>
          <div class="text-container">
          <h2>IF THEY SEE I’M A DONOR AT THE HOSPITAL, <span class="red">THEY WON’T TRY TO SAVE MY LIFE.</span></h2>
            <p>When you are sick or injured and admitted to a hospital, the one and only priority is to save your life. Period.<span class="green-text"> Donation doesn’t become a possibility until all lifesaving methods have failed.</span></p>
          </div>
        </div>

        <div class="myths-container">
          <div class="image-container">
            <img src="images/myth2@2x.png" alt="myth image">
          </div>
          <div class="text-container">
            <h2>PEOPLE WHO DONATE ORGANS <span class="red">CANNOT HAVE AN OPEN CASKET FUNERAL.</span></h2>
            <p>All recovery of donated organs and tissues is done with dignity, by surgeons who respect the body and use precise surgical skill to remove the organs. <span class="green-text">The procedure does not affect the appearance of the body, nor funeral practices</span>, and nobody will be able to tell that the deceased was an organ donor.</p>
          </div>
        </div>

        <div class="myths-container">
          <div class="image-container">
            <img src="images/myth3@2x.png" alt="myth image">
          </div>
          <div class="text-container">
          <h2>A PERSONS FINANCIAL OR CELEBRITY STATUS <span class="red">GIVES THEM PREFERENCE IN RECEIVING AN ORGAN.</span> </h2>
            <p>Trillium Gift of Life Network keeps a list of everyone in Ontario who is waiting for an organ and determines who gets an available organ. <span class="green-text">The severity of a patient’s illness, blood and tissue type match, and other medical information determine who gets the organ first.</span> If the medical urgency between two people is the same, the individual who’s been on the waiting list the longest will receive the organ.</p>
          </div>
        </div>

        <div class="myths-container">
          <div class="image-container">
            <img src="images/myth4@2x.png" alt="myth image">
          </div>
          <div class="text-container">
          <h2><span class="red">ONLY YOUNG PEOPLE IN GOOD HEALTH</span> CAN DONATE THEIR ORGANS.</h2>
            <p>Age is not as important as the health of the organs and tissue. The oldest Canadian organ donor so far was more than 90 years of age, and the oldest tissue donor was 102.
<span class="green-text">Even if a potential donor has had a serious illness such as cancer, that does not automatically exclude him/her from becoming a donor.</span> It depends on the type of illness, and when the person was treated. Ultimately, several factors are taken into consideration, such as the health of the organs and tissue at the time of death.</p>
          </div>
        </div>

        <div class="myths-container">
          <div class="image-container">
            <img src="images/myth5@2x.png" alt="myth image">
          </div>
          <div class="text-container">
          <h2>MY RELIGION <span class="red">DOES NOT ACCEPT ORGAN DONATION.</span></h2>
            <p><span class="green-text">Most major religions are in favour of organ donation.</span> If you are unsure, consult your faith leader. Restrictions may not apply if the donation could save another’s life.</p>
          </div>
        </div>


       </section>

    <FooterComponent />
   </section>

   `,
  data() {
    return {
      test: "test data"
    }
  },
  methods: {

  },
  components: {
    HeaderComponent: HeaderComponent,
    FooterComponent: FooterComponent
  }
}

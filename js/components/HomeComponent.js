import HeaderComponent from "./layout/HeaderComponent.js";
import FooterComponent from "./layout/FooterComponent.js";

export default {
  template: /*html*/`
    <section>
        <HeaderComponent />
        <section id="hero">
        <div class="hero-text">
          <h2>How will you</h2>
          <h1>Leave your mark?</h1>
          <p class="mobile-hide">We all want to make a difference, so let's make one that only takes 2 minutes. <span class="green-text">4500 people are waiting for an organ donation right now in Canada</span>. Debunk the myths, explore the facts, and register as a donor today.</p>
          <div class="btn-container">
            <a class="btn" href="https://www.beadonor.ca/community/toronto">Register</a>
            <img id="red-fprint" src="images/img_9.png" alt="Red fingerprint">
          </div>
          </div>
        </section>
        <img id="arrow-down" src="images/arrow_down.svg" alt="Arrow Down">
        <section id="videoSec" class="section-container">
          <h2 class="hidden">Organ Donation Infographic</h2>
          <img src="images/wordmark_logo.png" alt="Wordmark Logo">
          <div class="fullscreen-vid-wrap">
            <div class="vid-controls">
              <div class="ctr-wraper">
                <div class="icon" @click="volOff()">
                <i class="fas fa-volume-off fa-3x"></i>
                </div>
                <div class="icon" @click="volOn()">
                <i class="fas fa-volume-up fa-3x"></i>
                </div>
                <div class="icon" @click="restart()">
                <i class="fas fa-undo-alt fa-3x"></i>
                </div>
                <div class="icon" v-show="playing" @click="pause()">
                <i class="fas fa-pause fa-3x" ></i>
                </div>
                <div class="icon"v-show="!playing" @click="play">
                <i class="fas fa-play fa-3x"></i>
                </div>

                </div>
            </div>
            <video id="vid" autoplay muted loop>
            </video>
          </div>
        </section>
        <section id="thumbs" class="thumbnail-section section-container">
        <h2 class="hidden">Social Media / Events / Stories Thumbnail Section</h2>

        <div class="thumb-wrapper">
        <div class="social thumbnail-tile">
          <img id="teal-fprint" src="images/fprint_teal.svg" alt="Teal Fingerprint">
          <img class="thumb" src="images/social-tile.png" alt="Social Media Thumbnail">
          <h3>Media</h3>
          <p>Want to get involved in the Leave Your Mark online community? Are you a journalist interested in covering one of our events? You can learn more about our social media and brand here.</p>
          <a class="btn" href="#">Share and experience</a>
        </div>
        <div class="events thumbnail-tile">
        <img class="thumb" src="images/event-tile.png" alt="Events Thumbnail">
          <h3>Events</h3>
          <p>We believe meeting people where they're at is key to helping them leave their mark - and we want to see you in person! Find out about our outreach events to participate in the community.</p>
          <a class="btn" href="#">make plans</a>
        </div>
        <div class="stories thumbnail-tile">
        <img class="thumb" src="images/stories-tile.png" alt="Stories Thumbnail">
          <h3>Stories</h3>
          <p>The first step to understanding any issue is to listen with an open heart. Witness the stories of people whose lives have been impacted by organ wait lists and organ donations.</p>
          <a class="btn" href="#">Read More</a>

        </div>
        </div>

        </section>
        <!--<section id="myths" class="section-container">
          <h2 class="hidden">Myths section</h2>
          <div>
            <h3>did you<span>know?</span></h3>
            <div class="slider">
            <v-carousel>
              <v-carousel-item
                v-for="(item,i) in items"
                :key="i"
                :src="item.src"
              ></v-carousel-item>
            </v-carousel>
            </div>
          </div>
        </section>-->

        <section id="donate" class="section-container">
          <div class="content">
            <div class="donate-text">
            <h2>how can I <span>leave my mark?</span></h2>
            <p>Go to beadonor.org to register as an organ donor to make a difference in someone's life. The process is quick and easy. You just need
              <ul>
                <li>Your health card</li>
                <li>Your birthday date</li>
                <li>And two minutes</li>
              </ul>
            </p>
            <div class="btn-container">
            <a class="btn" href="https://www.beadonor.ca/community/toronto">register</a>
            </div>
            </div>
            <div id="polaroid">
              <img id="red-heart" src="images/logo_d3_red.png" alt="">
            </div>
           
          </div>
        </section>
        <FooterComponent />
    </section>
  `,

  data() {
    return {
      vid: null,
      playing: true,
      items: [
        {
          src: 'images/test_1.jpg'
        },
        {
          src: 'images/test_2.jpg'
        },
        {
          src: 'images/test_3.jpg'
        },
        {
          src: 'images/test_4.jpg'
        }
      ]
    };
  },
  created: function () {
    // this.vid = document.querySelector('#vid');
  },
  mounted: function () {
    this.vid = document.querySelector('#vid');
    // console.log(this.vid);
    this.showVideo();
    this.animOnScroll();


  },
  methods: {
    animOnScroll() {
      window.addEventListener('scroll', function () {
        // console.log(window.scrollY);
        //get img

        let heart = document.querySelector('#red-heart');
        let print = document.querySelector('#red-fprint');
        let tealPrint = document.querySelector('#teal-fprint');
        //Change img opacity on scroll

        if (window.scrollY > 15) {
          print.classList.add("fade-anim");
          print.style.opacity = 1;
        }
        if (window.scrollY > 1410) {
          tealPrint.classList.add("fade-anim");
          tealPrint.style.opacity = 1;
        }
        if (window.scrollY < 2250) {
          heart.style.opacity = 0;
        }
        if (window.scrollY > 2250) {
          heart.classList.add("fade-anim");
          heart.style.opacity = 1;
        }
      });
    },
    showVideo() {
      var vid = document.querySelector('#vid');

      // create source video element according to screen size
      if (window.matchMedia("(max-width: 700px)").matches) {
        this.videoSource(vid, "video/video_mobile.mp4", "video/mp4");

      } else if (window.matchMedia("(max-width: 999px)").matches) {
        this.videoSource(vid, "video/video_desktop.mp4", "video/mp4");

      } else if (window.matchMedia("(min-width: 1000px)").matches) {
        this.videoSource(vid, "video/video_desktop.mp4", "video/mp4");

      }
    },

    videoSource(element, src, type) {
      var source = document.createElement("source");

      source.src = src;
      source.type = type;

      element.appendChild(source);
    },

    volOn() {
      // console.log('volOn');
      this.vid.muted = false;
    },

    volOff() {
      // console.log('volOff');
      this.vid.muted = true;
    },
    play() {
      this.vid.play();
      this.playing = true;
      // console.log('play clicked');
    },
    pause() {
      this.vid.pause();
      this.playing = false;
      // console.log('pause clicked');
    },
    restart() {
      this.vid.pause();
      this.vid.currentTime = 0;
      this.vid.play();
    }
  },

  components: {
    HeaderComponent: HeaderComponent,
    FooterComponent: FooterComponent
  }
};

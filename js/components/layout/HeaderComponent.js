import NavHeaderComponent from "./NavHeaderComponent.js";
export default {
  template: /*html*/`
    <header>
   <a href="" @click.prevent="$router.push({ path: '/home' })"><img id="logo" src="images/logo_solid.png" alt="Organ Donation Logo"></a>
      <navheadercomponent ></navheadercomponent>
    </header>
  `,

  data() {
    return {

    };
  },

  methods: {},

  components: {
    navheadercomponent: NavHeaderComponent
  }
};

export default {
  template: /* html*/`
    <footer>
      <div class="footercontainer">

        <div class="actioncontainer">
          <h2>How will you leave your mark on the world?</h2>
          <p>Become and organ donor today and make a difference that will last a lifetime.</p>
          <div class="btn-container">
            <a class="btn" href="https://www.beadonor.ca/community/toronto">Register</a>
          </div>
        </div>

        <div class="footer-bottom">
          <div class="imagecontainer">
            <img src="images/wordmark_logo_white.png" alt="logo">
          </div>

          <div class="socialcontainer">
            <a href="#" class="socialicon"><i class="fab fa-facebook"></i></a>
            <a href="#" class="socialicon"><i class="fab fa-instagram"></i></a>
            <a href="#" class="socialicon"><i class="fab fa-twitter-square"></i></a>
            <a href="#" class="socialicon"><i class="fab fa-youtube-square"></i></a>
            <a href="#" class="socialicon"><i class="fab fa-medium"></i></a>
          </div>
        </div>

      </div>
    </footer>
  `
}

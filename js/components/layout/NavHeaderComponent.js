export default {

  template: /*html */`
         <div id="mainHeader">
          <h2 class="hidden">Hamburger Navigation</h2>
          <!-- Hamburger Navigation -->
          <div class="navigation">
            <input type="checkbox" class="navigation__checkbox" id="navi-toggle" />
            <label for="navi-toggle" class="navigation__button">
              <span class="navigation__icon">&nbsp;</span>
            </label>
            <div class="navigation__background">&nbsp;</div>
            <nav class="navigation__nav">
              <h3 class="hidden">Main Navigation</h3>
              <ul class="navigation__list">
                <li class="navigation__item"><a @click.prevent="$router.push({ path: '/home' })" class="navigation__link" href="">HOME</a></li>
                <li class="navigation__item"><a @click.prevent="$router.push({ path: '/stories' })" class="navigation__link" href="">STORIES</a></li>
                <li class="navigation__item"><a @click.prevent="$router.push({ path: '/myths' })" class="navigation__link" href="">MYTHS</a></li>
                <li class="navigation__item"><a @click.prevent="$router.push({ path: '/events' })" class="navigation__link" href="">EVENTS</a></li>
                <li class="navigation__item"><a @click.prevent="$router.push({ path: '/media' })" class="navigation__link" href="">MEDIA</a></li>
                <li class="navigation__item"><a class="navigation__link" href="https://www.beadonor.ca/community/toronto">REGISTER</a></li>
                <ul id="social">
                  <li>
                            <a href="http://www.twitter.com" target="_blank" id="tw"
                              ><i class="fab fa-twitter"></i
                            ></a>
                          </li>
                  <li>
                            <a href="http://www.instagram.com" target="_blank" id="pt"
                              ><i class="fab fa-instagram"></i
                            ></a>
                          </li>
                  <li>
                            <a href="http://www.facebook.com" target="_blank" id="fb"
                              ><i class="fab fa-facebook-square"></i
                            ></a>
                          </li>
                          <li>
                            <a href="http://www.snapchat.com" target="_blank" id="sc"
                              ><i class="fab fa-snapchat-square"></i></a>
                          </li>
                </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
    `
};

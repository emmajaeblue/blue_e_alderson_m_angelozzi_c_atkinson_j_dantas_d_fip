import HomeComponent from "./components/HomeComponent.js";
import EventsComponent from "./components/EventsComponent.js";
import EventDetailsComponent from "./components/EventDetailsComponent.js";
import StoriesComponent from "./components/StoriesComponent.js";
import StoryComponent from "./components/StoryComponent.js";
import MediaComponent from "./components/MediaComponent.js";
import MythsComponent from "./components/MythsComponent.js";

const routes = [{
  path: '/',
  name: "home",
  redirect: '/home',
  component: HomeComponent
},
{
  path: "/home",
  name: "home",
  component: HomeComponent
},
{
  path: "/events",
  name: "events",
  component: EventsComponent
},
{
  path: "/details",
  name: "details",
  props: true,
  component: EventDetailsComponent
},
{
  path: "/stories",
  name: "stories",
  component: StoriesComponent
},
{
  path: "/stories/:id",
  name: "story",
  props: true,
  component: StoryComponent
},
{
  path: "/media",
  name: "media",
  props: true,
  component: MediaComponent
},
{
  path: "/myths",
  name: "myths",
  component: MythsComponent
}
];

const router = new VueRouter({
  // mode: 'history',
  routes
});

const vm = new Vue({
  data: {

  },
  created: function () {
    window.onbeforeunload = function () {
      window.scrollTo(0, 0);
    };
  },
  watch: {
    $route: function () {

      // Check if given route is true, if it is then hide Lottie.
      // if (this.$route.path != "/") {
      this.$router.go(this.$route)
      // }
    }
  },
  mounted: function () {
  },
  methods: {
  },

  router: router
}).$mount("#app");

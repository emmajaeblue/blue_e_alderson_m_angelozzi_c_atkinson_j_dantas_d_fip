# Leave Your Mark - Organ Donation Ontario

###### Final Integrated Project

### Team

- PROJECT MANAGER: Emma Blue
- FRONT-END WEB DEVELOPER: Daniela Dantas
- BACK-END WEB DEVELOPER: Camillo Angelozzi
- MOTION/3D ARTIST: Jacob Atkinson
- GRAPHIC DESIGNER: Alderson Mariah

### Project Overview:

![image](images/screenshot.png)
The main objective of this project is for teams of students to pitch, design and develop a professional
advertising campaign and website for the Organ Donation Project.

### Initial Ideas:

![image](images/idea_logo.png)
![image](images/idea1.png)

### Features:

- Lottie Animation
- Infographic Video
- Interview Video
- CMS to Add Myths, Stories and Events
- Custom Video Player

## Tools used during development phase:

- Adobe Illustrator, Photoshop, After Effects
- PHP/MySQL
- Gulp.js
- VueJS Single Page Components
- SASS
- Javascript Fetch API, to get data from backend database
- Fonts: Product Sans and Inconsolata
- https://airbnb.design/lottie/ for animation created in Adobe After Effects

### Giving credits for places that helped to do this project

- https://stackoverflow.com/
- https://airbnb.io/lottie/web/web.html
- https://sass-lang.com/
- https://www.friendly.app/?ref=lapaninja

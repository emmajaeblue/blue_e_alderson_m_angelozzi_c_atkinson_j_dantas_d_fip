<?php

require_once '../scripts/config.php';

confirm_logged_in();

if (isset($_POST['create_event'])) {
    try {
        $pdo = connect_to_db();

        $events_title = htmlspecialchars($_POST['events_title']);
        $events_content = htmlspecialchars($_POST['events_description']);
        $events_location = htmlspecialchars($_POST['events_location']);
        $events_date = date('Y-m-d', strtotime($_POST['events_date']));

        //! image file information
        $events_image_name = $_FILES['events_img']['name'];
        $events_image_temp = $_FILES['events_img']['tmp_name'];
        $events_image_size = $_FILES['events_img']['size'];
        $events_image_error = $_FILES['events_img']['error'];
        $events_image_type = $_FILES['events_img']['type'];

        // 1. check FILE extension
        $file_extension = strtolower(pathinfo($events_image_name, PATHINFO_EXTENSION));
        $accepted_extensions = array('gif', 'jpg', 'jpe', 'jpeg', 'png');
        if (!in_array($file_extension, $accepted_extensions)) {
            throw new Exception('Wrong file type!');
        }

        // 2. check FILE error
        if ($events_image_error !== 0) {
            throw new Exception('Error in uploading, file size can be too big!');
        }

        // 3. assign FILE unique name (based on microsecond actual timeformat)
        $events_image = time() . '_' . rand(1000, 9999) . "." . $file_extension;

        // 4. resize image
        $folderPath = "../../images/thumbs/";
        $sourceProperties = getimagesize($events_image_temp);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:

                $imageResourceId = imagecreatefrompng($events_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagepng($targetLayer, $folderPath . "th_" . $events_image);

                $event_resized_image = "th_" . $events_image;

                break;

            case IMAGETYPE_GIF:

                $imageResourceId = imagecreatefromgif($events_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagegif($targetLayer, $folderPath . "th_" . $events_image);

                $event_resized_image = "th_" . $events_image;

                break;

            case IMAGETYPE_JPEG:

                $imageResourceId = imagecreatefromjpeg($events_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($targetLayer, $folderPath . "th_" . $events_image);

                $event_resized_image = "th_" . $events_image;

                break;

            default:

                echo "Invalid Image type.";

                exit;

                break;

        }

        // move img from temporary location to images folder
        move_uploaded_file($events_image_temp, "../../images/$events_image");

        $query = "INSERT INTO tbl_events(events_title, events_description, events_location, events_date, events_img, events_resized_img) ";
        $query .= "VALUES( :events_title, :events_content, :events_location, :events_date,  :events_image, :events_resized_img) ";

        $add_event = $pdo->prepare($query);
        $add_event->execute(
            array(
                ':events_title' => $events_title,
                ':events_content' => nl2br($events_content),
                ':events_location' => $events_location,
                ':events_date' => $events_date,
                ':events_image' => $events_image,
                ':events_resized_img' => $event_resized_image,
            )
        );
        redirect_to('event_page.php');
    } catch (Exception $e) {
        $msg = $e->getMessage();
        echo "<p style='color: red;'>Message: $msg</p>";
    }
}

?>
<form action="" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="events_title">Event Title</label>
    <input type="text" name="events_title" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="events_body">Event Content</label>
    <textarea name="events_description" class="form-control" id="event_content" cols="30" rows="10" required></textarea>
  </div>

  <div class="form-group">
    <label for="events_date">Event Date</label>
    <input type="date" name="events_date" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="events_location">Event Location</label>
    <input type="text" name="events_location" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="events_img">Event Image</label>
    <input type="file" name="events_img" class="form-control" required>
  </div>

  <div class="form-group">
    <input type="submit" name="create_event" class="btn btn-primary" value="Publish Event">
  </div>

</form>

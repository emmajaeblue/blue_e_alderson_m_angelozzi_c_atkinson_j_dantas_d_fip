<?php

require_once '../scripts/config.php';
confirm_logged_in();

// DELETE USER QUERY
if (isset($_GET['delete'])) {
    $pdo = connect_to_db();

    $deleted_event = $_GET['delete'];

    $img_query = "SELECT events_img, events_resized_img FROM tbl_events WHERE `events_id` = :delete_event";
    $img_to_delete = $pdo->prepare($img_query);
    $img_to_delete->execute(
        array(
        ':delete_event' => $deleted_event
      )
    );

    $images = [];
    while ($row = $img_to_delete->fetch(PDO::FETCH_ASSOC)) {
        $images[] = $row;
    };

    // delete img and thumbs
    unlink('../../images/' . $images[0]['events_img']);
    unlink('../../images/thumbs/' . $images[0]['events_resized_img']);



    $query = "DELETE FROM tbl_events WHERE `events_id` = :delete_event";
    $delete_event = $pdo->prepare($query);
    $delete_event->execute(
        array(
        ':delete_event' => $deleted_event
      )
    );
    redirect_to('event_page.php');
}


?>

<!-- Display Users -->
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>Event id</th>
      <th>Event Title</th>
      <th>Event Description</th>
      <th>Location</th>
      <th>Date</th>
      <th>Event Image</th>
      <th>edit</th>
      <th>delete</th>
    </tr>
  </thead>
  <tbody>

    <?php
    // FIND events FROM DATABASE

      require_once '../scripts/config.php';
      confirm_logged_in();

      $pdo = connect_to_db();

      $query = "SELECT COUNT(*) FROM tbl_events";
      $events_set = $pdo->prepare($query);
      $events_set->execute();

      if ($events_set->fetchColumn() > 0) {
          $query = "SELECT * FROM tbl_events";
          $get_events = $pdo->prepare($query);
          $get_events->execute();

          $events = [];

          while ($row = $get_events->fetch(PDO::FETCH_ASSOC)) {
              $event_id = $row['events_id'];
              $event_title = $row['events_title'];
              $event_body = $row['events_description'];
              $event_location = $row['events_location'];
              $event_date = $row['events_date'];
              $event_img = $row['events_img'];
              echo "<tr>";
              echo "<td>{$event_id}</td>";
              echo "<td>{$event_title}</td>";
              echo "<td>{$event_body}</td>";
              echo "<td>{$event_location}</td>";
              echo "<td>{$event_date}</td>";
              echo "<td><img width='100' src='../../images/$event_img' alt='Events Image'></td>";
              echo "<td><a href='event_page.php?source=edit_event&e_id={$event_id}'>Edit</a></td>";
              echo "<td><a href='event_page.php?delete={$event_id}'>Delete</a></td>";
              echo "</tr>";
          }
      }
      ?>

  </tbody>
</table>
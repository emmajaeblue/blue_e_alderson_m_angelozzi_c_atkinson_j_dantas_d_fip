<?php

require_once '../scripts/config.php';
confirm_logged_in();

if (isset($_GET['e_id'])) {
    $get_events_id = $_GET['e_id'];
}

$query = "SELECT * FROM tbl_events WHERE events_id = :get_events_id";
$get_event = $pdo->prepare($query);
$get_event->execute(
    array(
        ':get_events_id' => $get_events_id,
    )
);

if ($get_event->rowCount()) {
    while ($row = $get_event->fetch(PDO::FETCH_ASSOC)) {
        $events_id = $row['events_id'];
        $events_title = $row['events_title'];
        $events_content = $row['events_description'];
        $events_location = $row['events_location'];
        $events_date = $row['events_date'];
        $events_image = $row['events_img'];
        $events_resized_image = $row['events_resized_img'];
    }
}

if (isset($_POST['edit_events'])) {

    // delete old image from directory when update
    unlink('../../images/' . $events_image);
    unlink('../../images/thumbs/' . $events_resized_image);

    try {
        $events_title = htmlspecialchars($_POST['events_title']);
        $events_content = htmlspecialchars($_POST['events_description']);
        $events_location = htmlspecialchars($_POST['events_location']);
        $events_date = date('Y-m-d', strtotime($_POST['events_date']));

        //! image file information
        $events_image_name = $_FILES['events_img']['name'];
        $events_image_temp = $_FILES['events_img']['tmp_name'];
        $events_image_size = $_FILES['events_img']['size'];
        $events_image_error = $_FILES['events_img']['error'];
        $events_image_type = $_FILES['events_img']['type'];

        // 1. check FILE extension
        $file_extension = strtolower(pathinfo($events_image_name, PATHINFO_EXTENSION));
        $accepted_extensions = array('gif', 'jpg', 'jpe', 'jpeg', 'png', 'svg');
        if (!in_array($file_extension, $accepted_extensions)) {
            throw new Exception('Wrong file type!');
        }

        // 2. check FILE error
        if ($events_image_error !== 0) {
            throw new Exception('Error in uploading, file size can be too big!');
        }

        // 3. assign FILE unique name (based on microsecond actual timeformat)
        $events_image = time() . '_' . rand(1000, 9999) . "." . $file_extension;

        // 4. resize image
        $folderPath = "../../images/thumbs/";
        $sourceProperties = getimagesize($events_image_temp);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:

                $imageResourceId = imagecreatefrompng($events_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagepng($targetLayer, $folderPath . "th_" . $events_image);

                $event_resized_image = "th_" . $events_image;

                break;

            case IMAGETYPE_GIF:

                $imageResourceId = imagecreatefromgif($events_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagegif($targetLayer, $folderPath . "th_" . $events_image);

                $event_resized_image = "th_" . $events_image;

                break;

            case IMAGETYPE_JPEG:

                $imageResourceId = imagecreatefromjpeg($events_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($targetLayer, $folderPath . "th_" . $events_image);

                $event_resized_image = "th_" . $events_image;

                break;

            default:

                echo "Invalid Image type.";

                exit;

                break;

        }

        // move img from temporary location to images folder
        move_uploaded_file($events_image_temp, "../../images/$events_image");

        // handle empty image field when editing
        if (empty($events_image)) {
            $query = "SELECT * FROM tbl_events WHERE events_id = :get_events_id";
            $pdo = connect_to_db();

            $get_event = $pdo->prepare($query);
            $get_event->execute(
                array(
                    ':get_events_id' => $get_events_id,
                )
            );

            while ($row = $get_event->fetch(PDO::FETCH_ASSOC)) {
                $events_image = $row['events_img'];
            }
        }

        $pdo = connect_to_db();

        $query = "UPDATE tbl_events SET ";
        $query .= "events_title = :events_title, ";
        $query .= "events_description = :events_content, ";
        $query .= "events_location = :events_location, ";
        $query .= "events_date = :events_date, ";
        $query .= "events_img =  :events_image, ";
        $query .= "events_resized_img =  :events_resized_image ";
        $query .= "WHERE events_id = :get_events_id ";

        $set_story = $pdo->prepare($query);
        $set_story->execute(
            array(
                ':events_title' => $events_title,
                ':events_content' => nl2br($events_content),
                ':events_location' => $events_location,
                ':events_date' => $events_date,
                ':events_image' => $events_image,
                ':events_resized_image' => $event_resized_image,
                ':get_events_id' => $get_events_id,
            )
        );
        redirect_to('event_page.php');
    } catch (Exception $e) {
        $msg = $e->getMessage();
        echo "<p style='color: red;'>Message: $msg</p>";
    }
}

?>


<form action="" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="events_title">Event Title</label>
    <input type="text" name="events_title" class="form-control"
      value="<?php echo $events_title ?>">
  </div>

  <div class="form-group">
    <img src="../../images/<?php echo $events_image; ?>"
      alt="events image" width="100">
    <input type="file" name="events_img" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="events_description">Event Description</label>
    <textarea name="events_description" class="form-control" id="" cols="30"
      rows="10"><?php echo $events_content ?></textarea>
  </div>

  <div class="form-group">
    <label for="events_date">Event Date</label>
    <input type="date" name="events_date" class="form-control value=" <?php echo $events_date ?>"
    required>
  </div>

  <div class="form-group">
    <label for="events_location">EventLocation</label>
    <input type="text" name="events_location" class="form-control"
      value="<?php echo $events_location ?>">
  </div>

  <div class="form-group">
    <input type="submit" name="edit_events" class="btn btn-primary" value="Edit events">
  </div>

</form>
<?php
require "scripts/config.php";

$pdo = connect_to_db();

$query = "SELECT * FROM tbl_events";
$get_events = $pdo->prepare($query);
$get_events->execute();

$data = [];
while ($row = $get_events->fetch(PDO::FETCH_ASSOC)) {
    $data[] = $row;
}

echo json_encode($data);

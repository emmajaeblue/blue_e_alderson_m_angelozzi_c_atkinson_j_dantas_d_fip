<?php
require "scripts/config.php";

$pdo = connect_to_db();

$query = "SELECT * FROM tbl_stories";
$get_stories = $pdo->prepare($query);
$get_stories->execute();

$data = [];
while ($row = $get_stories->fetch(PDO::FETCH_ASSOC)) {
    $data[] = $row;
}

echo json_encode($data);

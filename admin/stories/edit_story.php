<?php

require_once '../scripts/config.php';
confirm_logged_in();

if (isset($_GET['s_id'])) {
    $get_stories_id = $_GET['s_id'];
}

$query = "SELECT * FROM tbl_stories WHERE stories_id = :get_stories_id";
$get_story = $pdo->prepare($query);
$get_story->execute(
    array(
        ':get_stories_id' => $get_stories_id,
    )
);

if ($get_story->rowCount()) {
    while ($row = $get_story->fetch(PDO::FETCH_ASSOC)) {
        $stories_id = $row['stories_id'];
        $stories_title = htmlspecialchars($row['stories_title']);
        $stories_content = htmlspecialchars($row['stories_body']);
        $stories_image = $row['stories_img'];
        $stories_resized_image = $row['stories_resized_img'];
        $stories_date = $row['stories_date'];
        $stories_person_name = htmlspecialchars($row['stories_person_name']);
        $stories_person_age = htmlspecialchars($row['stories_person_age']);
        $stories_category = htmlspecialchars($row['stories_category']);
    }
}

if (isset($_POST['edit_stories'])) {

    // delete old image from directory when update
    unlink('../../images/' . $stories_image);
    unlink('../../images/thumbs/' . $stories_resized_image);

    try {
        $stories_title = $_POST['stories_title'];
        $stories_content = htmlspecialchars($_POST['stories_body']);

        //! image file information
        $stories_image_name = $_FILES['stories_img']['name'];
        $stories_image_temp = $_FILES['stories_img']['tmp_name'];
        $stories_image_size = $_FILES['stories_img']['size'];
        $stories_image_error = $_FILES['stories_img']['error'];
        $stories_image_type = $_FILES['stories_img']['type'];

        // 1. check FILE extension
        $file_extension = strtolower(pathinfo($stories_image_name, PATHINFO_EXTENSION));
        $accepted_extensions = array('gif', 'jpg', 'jpe', 'jpeg', 'png', 'svg');
        if (!in_array($file_extension, $accepted_extensions)) {
            throw new Exception('Wrong file type!');
        }

        // 2. check FILE error
        if ($stories_image_error !== 0) {
            throw new Exception('Error in uploading, file size can be too big!');
        }

        // 2. check FILE error
        if ($stories_image_error !== 0) {
            throw new Exception('Error in uploading, file size can be too big!');
        }

        // 3. assign FILE unique name (based on microsecond actual timeformat)
        $stories_image = time() . '_' . rand(1000, 9999) . "." . $file_extension;

        // 4. resize image
        $folderPath = "../../images/thumbs/";
        $sourceProperties = getimagesize($stories_image_temp);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:

                $imageResourceId = imagecreatefrompng($stories_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagepng($targetLayer, $folderPath . "th_" . $stories_image);

                $story_resized_image = "th_" . $stories_image;

                break;

            case IMAGETYPE_GIF:

                $imageResourceId = imagecreatefromgif($stories_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagegif($targetLayer, $folderPath . "th_" . $stories_image);

                $story_resized_image = "th_" . $stories_image;

                break;

            case IMAGETYPE_JPEG:

                $imageResourceId = imagecreatefromjpeg($stories_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($targetLayer, $folderPath . "th_" . $stories_image);

                $story_resized_image = "th_" . $stories_image;

                break;

            default:

                echo "Invalid Image type.";

                exit;

                break;

        }

        // move img from temporary location to images folder
        move_uploaded_file($stories_image_temp, "../../images/$stories_image");

        $stories_date = date('Y-m-d', strtotime($_POST['stories_date']));
        $stories_person_name = $_POST['stories_person_name'];
        $stories_person_age = $_POST['stories_person_age'];
        $stories_category = $_POST['stories_category'];

        // handle empty image field when editing
        if (empty($stories_image)) {
            $query = "SELECT * FROM tbl_stories WHERE stories_id = :get_stories_id";
            $pdo = connect_to_db();

            $get_story = $pdo->prepare($query);
            $get_story->execute(
                array(
                    ':get_stories_id' => $get_stories_id,
                )
            );

            while ($row = $get_story->fetch(PDO::FETCH_ASSOC)) {
                $stories_image = $row['stories_img'];
            }
        }

        $pdo = connect_to_db();

        $query = "UPDATE tbl_stories SET ";
        $query .= "stories_title = :stories_title, ";
        $query .= "stories_body = :stories_content, ";
        $query .= "stories_img =  :stories_image, ";
        $query .= "stories_resized_img =  :stories_resized_image, ";
        $query .= "stories_date = :stories_date, ";
        $query .= "stories_person_name = :stories_person_name, ";
        $query .= "stories_person_age = :stories_person_age, ";
        $query .= "stories_category = :stories_category ";
        $query .= "WHERE stories_id = :get_stories_id ";

        $set_story = $pdo->prepare($query);
        $set_story->execute(
            array(
                ':stories_title' => $stories_title,
                ':stories_content' => nl2br($stories_content),
                ':stories_image' => $stories_image,
                ':stories_resized_image' => $story_resized_image,
                ':stories_date' => $stories_date,
                ':stories_person_name' => $stories_person_name,
                ':stories_person_age' => $stories_person_age,
                ':stories_category' => $stories_category,
                ':get_stories_id' => $get_stories_id,
            )
        );

        redirect_to('story_page.php');
    } catch (Exception $e) {
        $msg = $e->getMessage();
        echo "<p style='color: red;'>Message: $msg</p>";
    }
}

?>


<form action="" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="stories_title">Stories Title</label>
    <input type="text" name="stories_title" class="form-control"
      value="<?php echo $stories_title ?>" required>
  </div>

  <div class="form-group">
    <img src="../../images/<?php echo $stories_image; ?>"
      alt="stories image" width="100">
    <input type="file" name="stories_img" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="stories_body">Stories Content</label>
    <textarea required name="stories_body" class="form-control" id="" cols="30"
      rows="10"><?php echo $stories_content ?></textarea>
  </div>

  <div class="form-group">
    <label for="stories_date">stories Date</label>
    <input type="date" name="stories_date" class="form-control value=" <?php echo $stories_date ?>"
    required>
  </div>

  <div class="form-group">
    <label for="stories_person_name">Stories Person Name</label>
    <input type="text" name="stories_person_name" class="form-control"
      value="<?php echo $stories_person_name ?>" required>
  </div>

  <div class="form-group">
    <label for="stories_person_age">Stories Person Age</label>
    <input type="text" name="stories_person_age" class="form-control"
      value="<?php echo $stories_person_age ?>" required>
  </div>

  <div class="form-group">
    <label for="stories_category">Stories Category</label>
    <input type="text" name="stories_category" class="form-control"
      value="<?php echo $stories_category ?>" required>
  </div>

  <div class="form-group">
    <input type="submit" name="edit_stories" class="btn btn-primary" value="Edit stories">
  </div>

</form>
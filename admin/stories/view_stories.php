<?php

require_once '../scripts/config.php';
confirm_logged_in();

// DELETE USER QUERY
if (isset($_GET['delete'])) {
    $pdo = connect_to_db();

    $deleted_story = $_GET['delete'];

    $img_query = "SELECT stories_img, stories_resized_img FROM tbl_stories WHERE `stories_id` = :deletestory";
    $img_to_delete = $pdo->prepare($img_query);
    $img_to_delete->execute(
        array(
            ':deletestory' => $deleted_story,
        )
    );

    $images = [];
    while ($row = $img_to_delete->fetch(PDO::FETCH_ASSOC)) {
        $images[] = $row;
    }
    ;

    // delete img and thumbs
    unlink('../../images/' . $images[0]['stories_img']);
    unlink('../../images/thumbs/' . $images[0]['stories_resized_img']);

    $query = "DELETE FROM tbl_stories WHERE `stories_id` = :deletestory";
    $delete_story = $pdo->prepare($query);
    $delete_story->execute(
        array(
            ':deletestory' => $deleted_story,
        )
    );
    redirect_to('story_page.php');
}

?>

<!-- Display Users -->
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>Story id</th>
      <th>Title</th>
      <th>Image</th>
      <th>Description</th>
      <th>Date</th>
      <th>Person</th>
      <th>Person Age</th>
      <th>Category</th>
      <th>edit</th>
      <th>delete</th>
    </tr>
  </thead>
  <tbody>

    <?php
// FIND STORIES FROM DATABASE

require_once '../scripts/config.php';
confirm_logged_in();

$pdo = connect_to_db();

$query = "SELECT COUNT(*) FROM tbl_stories";
$stories_set = $pdo->prepare($query);
$stories_set->execute();

if ($stories_set->fetchColumn() > 0) {
    $query = "SELECT * FROM tbl_stories";
    $get_stories = $pdo->prepare($query);
    $get_stories->execute();

    $stories = [];

    while ($row = $get_stories->fetch(PDO::FETCH_ASSOC)) {
        $story_id = $row['stories_id'];
        $story_title = $row['stories_title'];
        $story_img = $row['stories_img'];
        $story_body = $row['stories_body'];
        $story_date = $row['stories_date'];
        $story_person_name = $row['stories_person_name'];
        $story_person_age = $row['stories_person_age'];
        $story_category = $row['stories_category'];
        echo "<tr>";
        echo "<td>{$story_id}</td>";
        echo "<td>{$story_title}</td>";
        echo "<td><img width='100' src='../../images/$story_img' alt='Stories Image'></td>";
        echo "<td>{$story_body}</td>";
        echo "<td>{$story_date}</td>";
        echo "<td>{$story_person_name}</td>";
        echo "<td>{$story_person_age}</td>";
        echo "<td>{$story_category}</td>";
        echo "<td><a href='story_page.php?source=edit_story&s_id={$story_id}'>Edit</a></td>";
        echo "<td><a href='story_page.php?delete={$story_id}'>Delete</a></td>";
        echo "</tr>";
    }
}
?>

  </tbody>
</table>
<?php

require_once '../scripts/config.php';

confirm_logged_in();

if (isset($_POST['create_story'])) {
    try {
        $pdo = connect_to_db();

        $stories_title = $_POST['stories_title'];

        //! image file information
        $stories_image_name = $_FILES['stories_img']['name'];
        $stories_image_temp = $_FILES['stories_img']['tmp_name'];
        $stories_image_size = $_FILES['stories_img']['size'];
        $stories_image_error = $_FILES['stories_img']['error'];
        $stories_image_type = $_FILES['stories_img']['type'];

        // 1. check FILE extension
        $file_extension = strtolower(pathinfo($stories_image_name, PATHINFO_EXTENSION));
        $accepted_extensions = array('gif', 'jpg', 'jpe', 'jpeg', 'png');
        if (!in_array($file_extension, $accepted_extensions)) {
            throw new Exception('Wrong file type!');
        }

        // 2. check FILE error
        if ($stories_image_error !== 0) {
            throw new Exception('Error in uploading, file size can be too big!');
        }

        // 3. assign FILE unique name (based on microsecond actual timeformat)
        $stories_image = time() . '_' . rand(1000, 9999) . "." . $file_extension;

        // 4. resize image
        $folderPath = "../../images/thumbs/";
        $sourceProperties = getimagesize($stories_image_temp);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:

                $imageResourceId = imagecreatefrompng($stories_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagepng($targetLayer, $folderPath . "th_" . $stories_image);

                $story_resized_image = "th_" . $stories_image;

                break;

            case IMAGETYPE_GIF:

                $imageResourceId = imagecreatefromgif($stories_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagegif($targetLayer, $folderPath . "th_" . $stories_image);

                $story_resized_image = "th_" . $stories_image;

                break;

            case IMAGETYPE_JPEG:

                $imageResourceId = imagecreatefromjpeg($stories_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($targetLayer, $folderPath . "th_" . $stories_image);

                $story_resized_image = "th_" . $stories_image;

                break;

            default:

                echo "Invalid Image type.";

                exit;

                break;

        }

        // move img from temporary location to images folder
        move_uploaded_file($stories_image_temp, "../../images/$stories_image");

        $stories_content = htmlspecialchars($_POST['stories_body']);
        $stories_date = date('Y-m-d', strtotime($_POST['stories_date']));
        $stories_person_name = htmlspecialchars($_POST['stories_person_name']);
        $stories_person_age = htmlspecialchars($_POST['stories_person_age']);
        $stories_category = htmlspecialchars($_POST['stories_category']);

        $query = "INSERT INTO tbl_stories(stories_title, stories_body, stories_img, stories_resized_img, stories_date, stories_person_name, stories_person_age, stories_category) ";
        $query .= "VALUES( :stories_title, :stories_content, :stories_image, :stories_resized_img, :stories_date,  :stories_person_name,  :stories_person_age,  :stories_category) ";

        $add_story = $pdo->prepare($query);
        $add_story->execute(
            array(
                ':stories_title' => $stories_title,
                ':stories_content' => nl2br($stories_content),
                ':stories_image' => $stories_image,
                ':stories_resized_img' => $story_resized_image,
                ':stories_date' => $stories_date,
                ':stories_person_name' => $stories_person_name,
                ':stories_person_age' => $stories_person_age,
                ':stories_category' => $stories_category,
            )
        );
        redirect_to('story_page.php');
    } catch (Exception $e) {
        $msg = $e->getMessage();
        echo "<p style='color: red;'>Message: $msg</p>";
    }
}

?>
<form action="" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="stories_title">Story Title</label>
    <input type="text" name="stories_title" class="form-control">
  </div>

  <div class="form-group">
    <label for="stories_img">Story Image</label>
    <input type="file" name="stories_img" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="stories_body">Story Content</label>
    <textarea name="stories_body" class="form-control" id="story_content" cols="30" rows="10" required></textarea>
  </div>

  <div class="form-group">
    <label for="stories_date">Story Date</label>
    <input type="date" name="stories_date" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="stories_person_name">Story Person Name</label>
    <input type="text" name="stories_person_name" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="stories_person_age">Story Person Age</label>
    <input type="text" name="stories_person_age" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="stories_category">Story Category</label>
    <input type="text" name="stories_category" class="form-control" required>
  </div>

  <div class="form-group">
    <input type="submit" name="create_story" class="btn btn-primary" value="Publish Story">
  </div>

</form>

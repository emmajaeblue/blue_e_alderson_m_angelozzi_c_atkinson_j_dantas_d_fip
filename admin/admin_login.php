<?php

require_once 'scripts/config.php';

if (isset($_GET['psw_updated'])) {
    echo "Password has been updated. Try to Login with your new password.";
}

if (isset($_GET['failed'])) {
    // treat wrong password case:
    echo "<p style='color: red;'>Username or Password incorrect. Please try again!</p>";
}

if (!isset($_POST['username']) || empty($_POST['password'])) {
    $message = "Login Page - For Testing add below credentials";
} else {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $ip = $_SERVER['REMOTE_ADDR'];

    $message = login($username, $password, $ip);
}

?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Login</title>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" href="../css/login.css">
  <link rel="stylesheet" href="../css/main.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.min.js"></script>

</head>

<body>



  <div class="page">
    <?php if (!empty($message)): ?>
    <h4>
      <?php echo $message; ?>
    </h4>
    <?php endif; ?>
    <div>
      <h5>
        <strong>Admin</strong> Login test => Username:
        <span style="color: red">admin</span>; Password:
        <span style="color: red">123</span>
      </h5>
    </div>
    <div class="container">
      <div class="left">
        <div class="login">Login</div>
        <div class="eula">LOGIN to Organ Donation site ADMIN Dashboard</div>
      </div>
      <div class="right">
        <svg viewBox="0 0 320 300">
          <defs>
            <linearGradient inkscape:collect="always" id="linearGradient" x1="13" y1="193.49992" x2="307" y2="193.49992"
              gradientUnits="userSpaceOnUse">
              <stop style="stop-color:#ff00ff;" offset="0" id="stop876" />
              <stop style="stop-color:#ff0000;" offset="1" id="stop878" />
            </linearGradient>
          </defs>
          <path
            d="m 40,120.00016 239.99984,-3.2e-4 c 0,0 24.99263,0.79932 25.00016,35.00016 0.008,34.20084 -25.00016,35 -25.00016,35 h -239.99984 c 0,-0.0205 -25,4.01348 -25,38.5 0,34.48652 25,38.5 25,38.5 h 215 c 0,0 20,-0.99604 20,-25 0,-24.00396 -20,-25 -20,-25 h -190 c 0,0 -20,1.71033 -20,25 0,24.00396 20,25 20,25 h 168.57143" />
        </svg>
        <form action="admin_login.php" method="post" class='form'>
          <label class='label required' for='name'>Username</label>
          <input class='text-input' id='name' name='username' required type='text'>
          <label for="password">Password</label>
          <input type="password" id="password" name='password' required>
          <button class="button" type="submit" id="submit">Submit</button>
        </form>
      </div>
    </div>
  </div>

  <script src="../js/login.js"></script>
</body>

</html>
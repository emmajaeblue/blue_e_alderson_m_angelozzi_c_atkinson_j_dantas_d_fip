<?php
require "scripts/config.php";

$pdo = connect_to_db();

if (isset($_GET['id'])) {
    $query = "SELECT * FROM tbl_stories WHERE stories_id = :id";
    $get_stories = $pdo->prepare($query);
    $get_stories->execute(
        array(
            ':id' => $_GET['id'],
        )
    );

    $data = [];
    while ($row = $get_stories->fetch(PDO::FETCH_ASSOC)) {
        $data[] = $row;
    }
} else if (isset($_GET['category'])) {
    $query = "SELECT * FROM tbl_stories WHERE stories_category = :category";
    $get_related_stories = $pdo->prepare($query);
    $get_related_stories->execute(
        array(
            ':category' => $_GET['category'],
        )
    );

    $data = [];
    while ($row = $get_related_stories->fetch(PDO::FETCH_ASSOC)) {
        $data[] = $row;
    }
}

echo json_encode($data);

<?php
require_once '../scripts/config.php';
confirm_logged_in();
?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Dashboard Panel</title>
  <!-- Bootstrap Core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="../css/sb-admin.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- FONT AWESOME -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
  </script>
</head>

<body>
  <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand">Organ Donation Admin Dashboard</a>
      </div>

      <!-- Top Menu Items -->
      <ul class="nav navbar-right top-nav">
        <form action="../scripts/caller.php?caller_id=logout" method="post">
          <button type="submit" name="submit" class="btn btn-danger"
            style="margin-top: 8px; margin-right: 10px;">LOGOUT</button>
        </form>
      </ul>


      <!-- ADMIN SideBar -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
          <li>
            <a href="../index.php"><i class="fa fa-fw fa-home"></i> Home </a>
          </li>
          <li><a href="javascript:;" data-toggle="collapse" data-target="#stories_dropdown"><i class="far fa-newspaper"></i></i> Stories <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="stories_dropdown" class="collapse">
            <li>
                <a href="../stories/story_page.php"> View Stories </a>
              </li>
          <li>
            <a href="../stories/story_page.php?source=add_story"> Add Story </a>
          </li>
        </ul>
        </li>
        <li><a href="javascript:;" data-toggle="collapse" data-target="#events_dropdown"><i class="far fa-calendar-alt"></i> Events <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="events_dropdown" class="collapse">
              <li>
                <a href="../events/event_page.php"> View Events </a>
              </li>
        <li>
            <a href="../events/event_page.php?source=add_event"> Add Event </a>
          </li>
        </ul>
        </li>

        <li><a href="javascript:;" data-toggle="collapse" data-target="#myths_dropdown"><i class="fa fa-fw fa-list-ul"></i> Myths <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="myths_dropdown" class="collapse">
              <li>
                <a href="myth_page.php"> View Myths </a>
              </li>
        <li>
            <a href="myth_page.php?source=add_myth"> Add Myth </a>
          </li>
        </ul>
        </li>

        <li><a href="javascript:;" data-toggle="collapse" data-target="#users_dropdown"><i class="fas fa-users"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="users_dropdown" class="collapse">
              <li>
                <a href="../users/user_page.php"> View Users </a>
              </li>
        <li>
            <a href="../users/user_page.php?source=add_user" title=""> Add User </a>
          </li>
        </ul>
        </li>

        </ul>
      </div>
      <!-- ADMIN SideBar end -->
    </nav>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">Welcome to ADMIN Dashboard</h1>
            <img src="../../images/logo_solid.png" alt="logo" id="logo">

            <!-- GET request for CRUD in myths -->
            <?php myths_crud();?>

          </div>
        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- jQuery -->
  <script src="../js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="../js/bootstrap.min.js"></script>
</body>

</html>
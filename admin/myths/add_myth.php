<?php

require_once '../scripts/config.php';

confirm_logged_in();

if (isset($_POST['create_myth'])) {
    try {
        $pdo = connect_to_db();

        $myths_title = htmlspecialchars($_POST['myths_title']);

        //! image file information
        $myths_image_name = $_FILES['myths_img']['name'];
        $myths_image_temp = $_FILES['myths_img']['tmp_name'];
        $myths_image_size = $_FILES['myths_img']['size'];
        $myths_image_error = $_FILES['myths_img']['error'];
        $myths_image_type = $_FILES['myths_img']['type'];

        // 1. check FILE extension
        $file_extension = strtolower(pathinfo($myths_image_name, PATHINFO_EXTENSION));
        $accepted_extensions = array('gif', 'jpg', 'jpe', 'jpeg', 'png');
        if (!in_array($file_extension, $accepted_extensions)) {
            throw new Exception('Wrong file type!');
        }

        // 2. check FILE error
        if ($myths_image_error !== 0) {
            throw new Exception('Error in uploading, file size can be too big!');
        }

        // 3. assign FILE unique name (based on microsecond actual timeformat)
        $myths_image = time() . '_' . rand(1000, 9999) . "." . $file_extension;

        // 4. resize image
        $folderPath = "../../images/thumbs/";
        $sourceProperties = getimagesize($myths_image_temp);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:

                $imageResourceId = imagecreatefrompng($myths_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagepng($targetLayer, $folderPath . "th_" . $myths_image);

                $myth_resized_image = "th_" . $myths_image;

                break;

            case IMAGETYPE_GIF:

                $imageResourceId = imagecreatefromgif($myths_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagegif($targetLayer, $folderPath . "th_" . $myths_image);

                $myth_resized_image = "th_" . $myths_image;

                break;

            case IMAGETYPE_JPEG:

                $imageResourceId = imagecreatefromjpeg($myths_image_temp);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($targetLayer, $folderPath . "th_" . $myths_image);

                $myth_resized_image = "th_" . $myths_image;

                break;

            default:

                echo "Invalid Image type.";

                exit;

                break;

        }

        // move img from temporary location to images folder
        move_uploaded_file($myths_image_temp, "../../images/$myths_image");

        $myths_content = htmlspecialchars($_POST['myths_desc']);

        $query = "INSERT INTO tbl_myths(myths_title, myths_desc, myths_img, myths_resized_img) ";
        $query .= "VALUES( :myths_title, :myths_content, :myths_image, :myths_resized) ";

        $add_story = $pdo->prepare($query);
        $add_story->execute(
            array(
                ':myths_title' => $myths_title,
                ':myths_content' => nl2br($myths_content),
                ':myths_image' => $myths_image,
                ':myths_resized' => $myth_resized_image,
            )
        );
        redirect_to('myth_page.php');
    } catch (Exception $e) {
        $msg = $e->getMessage();
        echo "<p style='color: red;'>Message: $msg</p>";
    }
}

?>
<form action="" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="myths_title">Myth Title</label>
    <input type="text" name="myths_title" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="myths_img">Myth Image</label>
    <input type="file" name="myths_img" class="form-control" required>
  </div>

  <div class="form-group">
    <label for="myths_desc">Myth Description</label>
    <textarea name="myths_desc" class="form-control" id="myth_content" cols="30" rows="10" required></textarea>
  </div>

  <div class="form-group">
    <input type="submit" name="create_myth" class="btn btn-primary" value="Publish Myth" required>
  </div>

</form>

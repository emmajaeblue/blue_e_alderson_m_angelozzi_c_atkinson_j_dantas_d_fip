<?php

require_once '../scripts/config.php';
confirm_logged_in();

// DELETE USER QUERY
if (isset($_GET['delete'])) {
    $pdo = connect_to_db();

    $deleted_myth = $_GET['delete'];

    $img_query = "SELECT myths_img, myths_resized_img FROM tbl_myths WHERE `myths_id` = :deletemyth";
    $img_to_delete = $pdo->prepare($img_query);
    $img_to_delete->execute(
        array(
        ':deletemyth' => $deleted_myth
      )
    );

    $images = [];
    while ($row = $img_to_delete->fetch(PDO::FETCH_ASSOC)) {
        $images[] = $row;
    };

    // delete img and thumbs
    unlink('../../images/' . $images[0]['myths_img']);
    unlink('../../images/thumbs/' . $images[0]['myths_resized_img']);

    $query = "DELETE FROM tbl_myths WHERE `myths_id` = :deletemyth";
    $delete_myth = $pdo->prepare($query);
    $delete_myth->execute(
        array(
        ':deletemyth' => $deleted_myth
      )
    );
    redirect_to('myth_page.php');
}


?>

<!-- Display Users -->
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>Myth id</th>
      <th>Myth Title</th>
      <th>Myth Description</th>
      <th>Myth Image</th>
      <th>edit</th>
      <th>delete</th>
    </tr>
  </thead>
  <tbody>

    <?php
      // FIND myths FROM DATABASE

        require_once '../scripts/config.php';
        confirm_logged_in();

        $pdo = connect_to_db();

        $query = "SELECT COUNT(*) FROM tbl_myths";
        $myths_set = $pdo->prepare($query);
        $myths_set->execute();

        if ($myths_set->fetchColumn() > 0) {
            $query = "SELECT * FROM tbl_myths";
            $get_myths = $pdo->prepare($query);
            $get_myths->execute();

            $myths = [];

            while ($row = $get_myths->fetch(PDO::FETCH_ASSOC)) {
                $myth_id = $row['myths_id'];
                $myth_title = $row['myths_title'];
                $myth_body = $row['myths_desc'];
                $myth_img = $row['myths_img'];
                echo "<tr>";
                echo "<td>{$myth_id}</td>";
                echo "<td>{$myth_title}</td>";
                echo "<td><img width='100' src='../../images/$myth_img' alt='Myths Image'></td>";
                echo "<td>{$myth_body}</td>";
                echo "<td><a href='myth_page.php?source=edit_myth&m_id={$myth_id}'>Edit</a></td>";
                echo "<td><a href='myth_page.php?delete={$myth_id}'>Delete</a></td>";
                echo "</tr>";
            }
        }
        ?>

  </tbody>
</table>
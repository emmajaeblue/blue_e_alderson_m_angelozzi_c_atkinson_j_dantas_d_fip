<?php

require_once 'config.php';

error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");

/* ---------------------------------------- */
/* ------------- CONNECT TO DB ------------ */
/* ---------------------------------------- */

//! connection to Database
function connect_to_db()
{
    $database = new Database();
    $conn = $database->connect();
    return $conn;
}

/* ---------------------------------------- */
/* ------- GET SINGLE ELEMENT QUERY ------- */
/* ---------------------------------------- */

function getSingle($tbl, $col, $value)
{
    $pdo = connect_to_db();

    $querySingle = 'SELECT * FROM ' . $tbl . ' WHERE ' . $col . '=' . $value;
    $runSingle = $pdo->query($querySingle);

    if ($runSingle) {
        return $runSingle;
    } else {
        $error = 'There is a problem accessing the info';
        return $error;
    }
}

/* ---------------------------------------- */
/* ------------- LOGIN FUNCTIONS ---------- */
/* ---------------------------------------- */

//! Redirect to location
function redirect_to($location)
{
    if ($location != null) {
        header('Location:' . $location);
        exit();
    }
}

//! confirmation email to new user
function user_created_email($fname, $username, $password, $email)
{
    $toEmail = $email;
    $subject = 'Welcome to ' . $fname . ' - User Registration is completed.';
    $body = '<h2>New User information: </h2>
					<h4>Username</h4><p>' . $username . '</p>
					<h4>Password</h4><p>' . $password . '</p>
					<h4>Login url: </h4><a href="http://localhost/Angelozzi_C_3014_r2/admin/admin_login.php">Login Here</a>
				';
    // Email Headers
    $headers = "MIME-Version: 1.0" . "\n";
    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\n";

    // mail($toEmail, $subject, $body, $headers);

    if (!mail($toEmail, $subject, $body, $headers)) {
        // Failed
        $msg = 'Your email was not sent';
        echo $msg;
    }
}

//! create a CRYPTOGRAPHICALLY SECURE PSEUDORANDOM NUMBER GENERATOR
function random_password($length = 8)
{
    $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' .
        '0123456789';

    $str = '';
    $max = strlen($chars) - 1;

    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[random_int(0, $max)];
    }

    return $str;
}

//! update initial password
function update_first_login($old_password, $new_password, $username, $email, $fname)
{
    $pdo = connect_to_db();
    $get_password_query = "SELECT `user_password` FROM tbl_user WHERE user_name = :username";
    $get_password = $pdo->prepare($get_password_query);
    $get_password->execute(
        array(
            ':username' => $username,
        )
    );

    $init_password = $get_password->fetchColumn();

    if (password_verify($old_password, $init_password)) {
        $update_psw_query = 'UPDATE `tbl_user` SET `user_password` = "' . $new_password . '", `user_last_login` = NOW(), `user_fname` = :fname, `user_name` = :username, `user_email` = :email, `user_admin_role` = 1 WHERE user_id = :id';
        $set_new_password = $pdo->prepare($update_psw_query);
        $set_new_password->execute(
            array(
                ':id' => $_SESSION['user_id'],
                ':username' => $username,
                ':fname' => $fname,
                ':email' => $email,
            )
        );

        Header('Location: ../admin_login.php?psw_updated');
    } else {
        echo "Password has not been updated. Try again.";
    }
}

//! reset wrong login
function reset_wrong_login($username)
{
    $pdo = connect_to_db();
    // update user attempt login to zero
    $update_user_attempt_login = "UPDATE `tbl_user` SET `user_failed_login_attempts` = 0 WHERE user_name = :username";
    $user_failed_login = $pdo->prepare($update_user_attempt_login);
    $user_failed_login->execute(
        array(
            ':username' => $username,
        )
    );
}

//! Image Resize
function imageResize($imageResourceId, $width, $height)
{
    $targetWidth = 300;
    $targetHeight = 300;
    $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);

    imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);

    return $targetLayer;
}

/* ---------------------------------------- */
/* ---------- USERS CRUD FUNCTIONS -------- */
/* ---------------------------------------- */
//! ADMIN redirect to Users CRUD operations
function users_crud()
{
    $pdo = connect_to_db();
    if (isset($_GET['source'])) {
        $source = $_GET['source'];
    } else {
        $source = "";
    }

    switch ($source) {

        case 'add_user':
            include "../users/add_user.php";
            break;

        case 'edit_user':
            include "../users/edit_users.php";
            break;

        default:
            include "../users/view_users.php";
            break;

    }
}

//! Create User
function createUser($fname, $username, $password, $email, $random_not_hashed_password)
{
    require_once 'config.php';
    $pdo = connect_to_db();
    $query = "INSERT INTO tbl_user (`user_fname`, `user_name`, `user_password`, `user_email`) VALUES (:fname, :username, :password, :email)";

    $insert_user = $pdo->prepare($query);
    $insert_user->execute(
        array(
            ':fname' => $fname,
            ':username' => $username,
            ':password' => $password,
            ':email' => $email,
        )
    );

    // send confirmation email to new user
    user_created_email($fname, $username, $password, $email);

    if ($insert_user->rowCount() > 0) {
        // redirect_to('index.php');
        Header('Location: ../index.php?success= ' . $random_not_hashed_password);
    } else {
        $message = "Insert data failed!";
        return $message;
    }
}

/* ---------------------------------------- */
/* --------- STORIES CRUD FUNCTIONS ------- */
/* ---------------------------------------- */
//! ADMIN redirect to Stories CRUD operations
function stories_crud()
{
    $pdo = connect_to_db();
    if (isset($_GET['source'])) {
        $source = $_GET['source'];
    } else {
        $source = "";
    }

    switch ($source) {

        case 'add_story':
            include "../stories/add_story.php";
            break;

        case 'edit_story':
            include "../stories/edit_story.php";
            break;

        default:
            include "../stories/view_stories.php";
            break;

    }
}

/* ---------------------------------------- */
/* ---------- MYTHS CRUD FUNCTIONS -------- */
/* ---------------------------------------- */
//! ADMIN redirect to Myths CRUD operations
function myths_crud()
{
    $pdo = connect_to_db();
    if (isset($_GET['source'])) {
        $source = $_GET['source'];
    } else {
        $source = "";
    }

    switch ($source) {

        case 'add_myth':
            include "../myths/add_myth.php";
            break;

        case 'edit_myth':
            include "../myths/edit_myth.php";
            break;

        default:
            include "../myths/view_myths.php";
            break;

    }
}

/* ---------------------------------------- */
/* --------- EVENTS CRUD FUNCTIONS -------- */
/* ---------------------------------------- */
//! ADMIN redirect to Events CRUD operations
function events_crud()
{
    $pdo = connect_to_db();
    if (isset($_GET['source'])) {
        $source = $_GET['source'];
    } else {
        $source = "";
    }

    switch ($source) {

        case 'add_event':
            include "../events/add_event.php";
            break;

        case 'edit_event':
            include "../events/edit_event.php";
            break;

        default:
            include "../events/view_events.php";
            break;

    }
}

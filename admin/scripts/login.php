<?php

function login($username, $password, $ip)
{
    require_once 'config.php';
    $pdo = connect_to_db();

    //check if username exists
    $check_exist_query = "SELECT COUNT(*) FROM tbl_user WHERE `user_name` = :username AND `user_active` = 1";

    $user_set = $pdo->prepare($check_exist_query);
    $user_set->execute(
        array(
      ':username' => $username
      )
    );

    // check password
    $login_info_query = "SELECT * FROM tbl_user WHERE user_name = :username";
    $user_check = $pdo->prepare($login_info_query);
    $user_check->execute(
        array(
      ':username' => $username
    )
  );
    $user_info_check = $user_check->fetch(PDO::FETCH_ASSOC);

    // decrypt password
    if (!password_verify($password, $user_info_check['user_password']) && $user_info_check['user_name'] === $username) {
        $update_user_attempt_login = 'UPDATE `tbl_user` SET `user_failed_login_attempts` = (user_failed_login_attempts + 1) WHERE `user_id` = :id';
        $user_failed_login = $pdo->prepare($update_user_attempt_login);
        $user_failed_login->execute(
            array(
          ':id' => $user_info_check['user_id']
        )
      );

        if ($user_info_check['user_failed_login_attempts'] >= 2) {
            $update_user_active = "UPDATE `tbl_user` SET `user_active` = 0 WHERE user_name = :username";
            $user_inactive = $pdo->prepare($update_user_active);
            $user_inactive->execute(
                array(
          ':username' => $username
        )
      );
            redirect_to('blocked.php');
        } else {
            redirect_to('admin_login.php');
        }
    }

    //  -------- check FIRST LOGIN to update password ---------
    $isFirst_query = "SELECT * FROM tbl_user WHERE user_name = :username";
    $isFirstLogin = $pdo->prepare($isFirst_query);
    $isFirstLogin->execute(
        array(
      ':username' => $username
    )
  );
    $logging_user = $isFirstLogin->fetch(PDO::FETCH_ASSOC);
    if ($logging_user['user_last_login'] === null && password_verify($password, $logging_user['user_password'])) {
        $_SESSION['user_id'] = $logging_user['user_id'];
        redirect_to('first_login_edit.php');
    }


    //  -------- END check FIRST LOGIN to update password ---------

    // -------------- get user ip address
    // $host = gethostname();
    // $ip = gethostbyname($host);
    //
    // $update_user_ip = "UPDATE `tbl_user` SET `user_ip` = '$ip' WHERE `user_id` = $id;";
    //
    // $set_user_ip = $pdo->prepare($update_user_ip);
    // $set_user_ip->execute();
    $update_ip_query = "UPDATE `tbl_user` SET `user_ip` = :ip WHERE user_name = :username";
    $update_ip_set = $pdo->prepare($update_ip_query);
    $update_ip_set->execute(
        array(
      ':ip' => $ip,
      ":username" => $username
    )
  );
    //  ----------- END get user ip address

    // //  -------- check User role ---------
    // $user_role_query = "SELECT * FROM tbl_user WHERE `user_name` = :username AND `user_active` = 1;";
    // $user_role_check = $pdo->prepare($user_role_query);
    // $user_role_check->execute(
    //     array(
    //   ':username' => $username
    //   )
    // );
    // $user_data = $user_role_check->fetch(PDO::FETCH_ASSOC);

    // if ($user_data['user_admin_role'] < 1 && $user_data['user_name'] === $username) {
    //     reset_wrong_login($username);
    //     redirect_to('../../Angelozzi_C_3014_r2/index.php');
    // }
    //  -------- END check User role ---------
    if ($user_set->fetchColumn() > 0) {
        $get_user_query = "SELECT * FROM tbl_user WHERE `user_name` = :username";
        $get_user_set = $pdo->prepare($get_user_query);
        $get_user_set->execute(
            array(
        ":username" => $username
        )
      );

        while ($found_user = $get_user_set->fetch(PDO::FETCH_ASSOC)) {
            $id = $found_user['user_id'];
            $_SESSION['user_id'] = $id;
            $_SESSION['user_name'] = $found_user['user_name'];
            $_SESSION['user_admin_role'] = $found_user['user_admin_role'];
            $_SESSION['user_password'] = $found_user['user_password'];
            $_SESSION['user_active'] = $found_user['user_active'];

            reset_wrong_login($username);

            // get saved datetime from database new column 'user_last_login'
            $_SESSION['user_last_login'] = $found_user['user_last_login'];

            // set timezone to UTC
            date_default_timezone_set('UTC');

            // update datetime on database new column 'user_last_login'
            $set_login_datetime = 'UPDATE `tbl_user` SET `user_last_login` = NOW() WHERE `user_id` = ' .  $_SESSION['user_id'] . ';';
            $set_login_datetime = $pdo->prepare($set_login_datetime);
            $set_login_datetime->execute();
            // ---------------------------------------
        }
        if (password_verify($password, $_SESSION['user_password'])) {
            redirect_to('index.php');
        }
    } else {
        $message = 'Login Failed - Contact Admin for checking your access details.';
        return $message;
        ;
    }
}

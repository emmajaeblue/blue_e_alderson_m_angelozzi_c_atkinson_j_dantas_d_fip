<?php


if (isset($_POST['submit'])) {
    require_once 'config.php';

    $old_password = $_POST['oldpassword'];
    $new_password = $_POST['newpassword'];
    $new_password = password_hash($new_password, PASSWORD_DEFAULT);
    $username = $_POST['username'];
    $email = $_POST['email'];
    $fname = $_POST['firstname'];


    update_first_login($old_password, $new_password, $username, $email, $fname);
}

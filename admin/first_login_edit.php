<?php
  require 'scripts/config.php';
  $pdo = connect_to_db();
  confirm_logged_in();

  $id = $_SESSION['user_id'];

  $tbl = 'tbl_user';
  $col = 'user_id';

  $user_first_login = getSingle($tbl, $col, $id);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Create User</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/cosmo/bootstrap.min.css">
  <link rel="stylesheet" href="../admin/css/main.css">
</head>

<?php if (!empty($message)) : ?>
<h4><?php echo $message; ?>
</h4>
<?php endif; ?>

<body>
  <?php
    while ($row = $user_first_login->fetch(PDO::FETCH_ASSOC)) {
        $user_id = $row['user_id'];
        $user_fname = $row['user_fname'];
        $user_name = $row['user_name'];
        $user_password = $row['user_password'];
        $user_email = $row['user_email'];
    }
    ?>
  <h2>Welcome <?= $user_name; ?>
    - This is your First Login!</h2>
  <p style="color: red;">Please UPDATE YOUR PASSWORD, and review any details.</p>

  <form action="scripts/first_login_psw_change.php" method="post">
    <fieldset>
      <div class="form-group">
        <label for="firstname">First Name</label>
        <input type="text" class="form-control" id="username" aria-describedby="emailHelp" name="firstname" value="<?php echo $user_fname; ?>"
          style="width: 48%;">
      </div>
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" id="username" aria-describedby="emailHelp" name="username" value="<?php echo $user_name; ?>"
          style="width: 48%;">
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="username" aria-describedby="emailHelp" name="email" value="<?php echo $user_email; ?>"
          style="width: 48%;">
      </div>
      <div class="form-group">
        <label style="color: red;" for="oldpassword">Random 8 Chars Generated Password from Confirmation Email</label>
        <input type="password" class="form-control" id="oldpassword" placeholder="Old Password" name="oldpassword"
          value="" style="width: 48%;">
      </div>
      <div class="form-group">
        <label style="color: red;" for="newpassword">New Password</label>
        <input type="newpassword" class="form-control" id="newpassword" placeholder="New Password" name="newpassword"
          value="" style="width: 48%;">
      </div>

      <button type='submit' name='submit'>UPDATE PROFILE</button>
    </fieldset>
  </form>

</body>

</html>
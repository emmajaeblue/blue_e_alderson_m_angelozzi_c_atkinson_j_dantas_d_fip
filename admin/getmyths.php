<?php
require "scripts/config.php";

$pdo = connect_to_db();

$query = "SELECT * FROM tbl_myths";
$get_myths = $pdo->prepare($query);
$get_myths->execute();

$data = [];
while ($row = $get_myths->fetch(PDO::FETCH_ASSOC)) {
    $data[] = $row;
}

echo json_encode($data);

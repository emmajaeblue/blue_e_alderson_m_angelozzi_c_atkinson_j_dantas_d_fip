<?php

  require_once '../scripts/config.php';
  confirm_logged_in();

    if (isset($_GET['u_id'])) {
        $getUserId = $_GET['u_id'];
    }

    $query = "SELECT * FROM `tbl_user` WHERE `user_id` = :getUserId";
    $get_user = $pdo->prepare($query);
  $get_user->execute(
      array(
      ':getUserId' => $getUserId
    )
  );

    // if ($get_user->rowCount()) {
        while ($row = $get_user->fetch(PDO::FETCH_ASSOC)) {
            $user_id = $row['user_id'];
            $user_fname = $row['user_fname'];
            $user_name = $row['user_name'];
            $user_email = $row['user_email'];
            // $user_img = $row['user_img'];
            $user_active = $row['user_active'];
        }
    // }

    if (isset($_POST['edit_users'])) {
        $user_fname =  $_POST['user_fname'];
        $user_name =  $_POST['user_name'];

        $user_email =  $_POST['user_email'];
        $user_active =  $_POST['user_active'];

        $query = "UPDATE tbl_user SET ";
        $query .="user_fname = :user_fname, ";
        $query .="user_name = :user_name, ";
        $query .="user_email = :user_email, ";
        $query .="user_active = :user_active ";
        $query .= "WHERE `user_id` = :getUserId ;";

        $update_user = $pdo->prepare($query);
        $update_user->execute(
            array(
        ':user_fname' => $user_fname,
        ':user_name' => $user_name,
        ':user_email' => $user_email,
        ':user_active' => $user_active,
        ':getUserId' => $getUserId
      )
    );
        redirect_to('user_page.php');
    }

?>
<form action="" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="user_fname">Name</label>
    <input type="text" name="user_fname" class="form-control"
      value="<?php echo $user_fname ?>">
  </div>

  <div class="form-group">
    <label for="user_name">username</label>
    <input type="text" name="user_name" class="form-control"
      value="<?php echo $user_name ?>">
  </div>

  <div class="form-group">
    <label for="user_email">User Email</label>
    <input type="text" name="user_email" class="form-control"
      value="<?php echo $user_email ?>">
  </div>

  <div class="form-group">
    <label for="user_active">User Active</label>
    <input type="text" placeholder="1 = active; 0 = inactive" name="user_active" class="form-control"
      value="<?php echo $user_active ?>">
    <small class="form-text text-muted">Set: 1 = active; 0 = inactive</small>
  </div>


  <div class="form-group">
    <input type="submit" name="edit_users" class="btn btn-primary" value="Edit User">
  </div>

</form>
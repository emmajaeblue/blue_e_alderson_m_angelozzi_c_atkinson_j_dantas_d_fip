<?php

require_once '../scripts/config.php';
confirm_logged_in();

// DELETE USER QUERY
if (isset($_GET['delete'])) {
    $deleted_user = $_GET['delete'];
    $query = "DELETE FROM tbl_user WHERE `user_id` = :deleteuser";
    $delete_user = $pdo->prepare($query);
    $delete_user->execute(
      array(
        ':deleteuser' => $deleted_user
      )
    );
}


?>

<!-- Display Users -->
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>User id</th>
      <!-- <th>User Image</th> -->
      <th>Name</th>
      <th>username</th>
      <th>User Email</th>
      <th>User Active</th>
      <th>edit</th>
      <th>delete</th>
    </tr>
  </thead>
  <tbody>

    <?php
      // FIND USERS FROM DATABASE

        require_once '../scripts/config.php';
        confirm_logged_in();

        $query = "SELECT COUNT(*) FROM tbl_user";
        $users_set = $pdo->prepare($query);
        $users_set->execute();

        if ($users_set->fetchColumn() > 0) {
            $query = "SELECT * FROM tbl_user";
            $get_users = $pdo->prepare($query);
            $get_users->execute();

            $users = [];

            while ($row = $get_users->fetch(PDO::FETCH_ASSOC)) {
                $user_id = $row['user_id'];
                $user_fname = $row['user_fname'];
                $user_name = $row['user_name'];
                $user_email = $row['user_email'];
                // $user_img = $row['user_img'];
                $user_active = $row['user_active'];
                echo "<tr>";
                echo "<td>{$user_id}</td>";
                // echo "<td><img width='30' src='../../images/$user_img' alt='User Image'></td>";
                echo "<td>{$user_name}</td>";
                echo "<td>{$user_fname}</td>";
                echo "<td>{$user_email}</td>";
                echo "<td>{$user_active}</td>";
                echo "<td><a href='user_page.php?source=edit_user&u_id={$user_id}'>Edit</a></td>";
                echo "<td><a href='user_page.php?delete={$user_id}'>Delete</a></td>";
                echo "</tr>";
            }
        }
        ?>

  </tbody>
</table>